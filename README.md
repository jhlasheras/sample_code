## Readme

The notebooks and scripts here are supossed to be used as **sample tools**. Most of them will only run locally in SOCIB's servers, due to need for locally stored data.

#### projects

The work here shown has been developed mainly for data analysis in different projects, where SOCIB has been involved. Main results of these analysis and projects where included in the following scientific publications:

- Jaime Hernandez Lasheras Doctoral Thesis: https://dspace.uib.es/xmlui/handle/11201/159802?show=full

- https://os.copernicus.org/articles/14/1069/2018/

- https://os.copernicus.org/articles/17/1157/2021/

more on: https://www.researchgate.net/profile/Jaime-Hernandez-Lasheras/research
