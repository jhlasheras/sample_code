import warnings
warnings.filterwarnings('ignore')

from datetime import datetime, timedelta

import sys
sys.path.append('/home/jhernandez/Work/socib_modelling_python/')
sys.path.append('/home/jhernandez/Work/python_tools/')

from socib_modelling_python.batch import SimulationFactory
from socib_modelling_python.compute import lagrangian as mff_lagrangian
from socib_modelling_python.plot.mff_maps import plot_points_map

import xarray as xr
import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import griddata
import cmocean
import myio as io
from glob import glob

simulations = ['hindcast_mfsv17', 'hindcast_glorys3h', 'assim_generic', 'assim_totals_actual', 'assim_totals_future']
#simulations = [ 'assim_generic', 'assim_totals_actual', 'assim_totals_future']

for simulation_name in simulations:

    # Load Dataset
    #file_simulation = glob(F"/LOCALDATA/FSLE/fields/wmop_{simulation_name}_*_fields_20140920_20141020.nc")[0]
    file_simulation = glob(F"/LOCALDATA/FSLE/fields/wmop_{simulation_name}_*_fields_20140802_20140830.nc")[0]
    data = xr.open_dataset(file_simulation, chunks='auto')

    # Grid
    xx, yy = np.meshgrid( data.lon_rho.values, data.lat_rho.values)
    fsle_grid = {}

    ndays = 15
    date_end = datetime(2014,8,17)

    while date_end < datetime(2014,8,30):

        # Selec dates
        date_ini = date_end - timedelta(days=ndays)
        strdate = date_end.strftime('%Y%m%d')

        # Output
        output_filename = F"/LOCALDATA/FSLE/Outputs/Backward_15_days_fields/backwards_15_days_{strdate}_{simulation_name}.nc"

        # Setting mask
        da = data['u'].isel(ocean_time=0)
        land = np.isnan(da)
        data['land_mask'] = land
        land_coords = ['lat_uv', 'lon_uv']

        # Subset Dataset to target temporal coverage
        data_sub = data.sel(ocean_time=slice(date_ini.strftime('%Y-%m-%d'), date_end.strftime('%Y-%m-%d')) )

        specs = {
            "kind": "parcels",
            "fieldset": {
                "params": {
                    "field": data_sub,
                    "variables": {"U": "u", "V": "v"},
                    "dimensions": {"time": "ocean_time",
                                   "lat": "lat_uv", "lon": "lon_uv"},
                }
            },
            "particles": {
                "generator": {
                    "method": "associated_particles",
                    "params": {
                        "grid": data,
                        "latitude": 'lat_uv',
                        "longitude": 'lon_uv',
                        "land_mask": data["land_mask"],
                        "land_mask_coords": land_coords,
                        "region": "WesternMed",
                        "strategy": "4NN2D"
                    }
                },
                "pclass": "JITParticleAssociated",
                "pclass_config": {"n_associations": 4}
            },
            "kernels": {
                "sequence": ["AdvectionRK4"]
            },
            "strategies": {
                "boundaries": "DeleteParticle"
            },
            "execution": {
                "dt": {"hours": -3},
                "outputdt": {"hours": 3},
                "runtime": {"days": 15}
            }
        }

        print(F"Launching Parcels simulation for {strdate} experiment: {simulation_name}")

        simulation = SimulationFactory.produce(specs)
        generated_particles = simulation.get_all_particles_location()

        # Run simulation
        output_file = simulation.run(output_filename=output_filename)

        # Calculate FSLE
        simulation_result = xr.open_dataset(output_filename)

        fsle_result = mff_lagrangian.calculate_FSLE(output_file, delta_f=20_000, delta_t=3/24, aggregation="min")

        fsle_grid[strdate] = griddata( (generated_particles[1], generated_particles[0]), fsle_result, (xx, yy), method='nearest')

        date_end = date_end + timedelta(days=1)

    io.save_obj(fsle_grid, F"fsle_gridded_values_15day_backward_{strdate}_{simulation_name}", folder="/LOCALDATA/FSLE/Outputs/Backward_15_days_fsle/")

io.save_obj(xx, F"fsle_grid_xx", folder="/LOCALDATA/FSLE/Outputs/Backward_15_days_fsle/")
io.save_obj(yy, F"fsle_grid_yy", folder="/LOCALDATA/FSLE/Outputs/Backward_15_days_fsle/")
