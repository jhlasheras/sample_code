import sys
sys.path.append('/home/jhernandez/Work//')
sys.path.append('/home/jhernandez/Work/python_tools/')

from defaults import *
from tqdm import tqdm

from scipy.interpolate import griddata
from scipy import interpolate

simulations = {"CR": F"//mnt/vinson/DATA/modelling/WMOP_REANALYSIS/Outputs/EUROSEA_T4_FreeRun/forecast_scratch/",
               "noGLI": F"/mnt/vinson/DATA/modelling/WMOP_REANALYSIS/Outputs/EUROSEA_T4_noGLIDERS/forecast_scratch/",
               "GLI": F"/mnt/vinson/DATA/modelling/WMOP_REANALYSIS/Outputs/EUROSEA_T4_GLIDERS/forecast_scratch/"
              }

path_simulation = simulations['CR']


import pyroms
from scipy.interpolate import interp1d

file_date = glob(F"{simulations['CR']}*avg*")[0]

grd = pyroms.grid.get_ROMS_grid('grid', hist_file=file_date, grid_file=file_date)
zeta = grd.vgrid.z_r[:]

# Glider files path
path_glider_cmems = '/home/modelling/data/Data/Observations/GLIDERS/CMEMS_EuroSea_T4.2/monthly/'
files_glider = glob(F'{path_glider_cmems}*2017*')

df_total = None

for file in files_glider:
    
    ds_g = xr.open_dataset(file)

    if 'SOCIB' not in ds_g.institution:
        print(F"{file} not corresponding to SOCIB glider")
        continue
    else:
        print("")
        print(F" Interpolating WMOP for Glider {ds_g.platform_name}")
        # Get Glider data
        pres = ds_g.PRES.values
        temp = ds_g.TEMP.values
        temp_qc = ds_g.TEMP_QC.values
        salt = ds_g.PSAL.values
        salt_qc = ds_g.PSAL_QC.values

        nz = ds_g.DEPTH.shape[0]

        time = np.repeat(ds_g.TIME.values, nz)
        longitude = np.repeat(ds_g.LONGITUDE.values, nz)
        latitude = np.repeat(ds_g.LATITUDE.values, nz)

        ii = (~np.isnan(temp)) & (temp_qc==1) & (~np.isnan(pres)) 
        print(F"#obs = {ii[ii==True].shape}")
        
        t_1d = {}
        s_1d = {}

        for sim in simulations:

            path_simulation = simulations[sim]

            t_interp = np.empty_like( temp )
            s_interp = np.empty_like( salt )

            # for nt in tqdm(range(ds_g.TIME.shape[0]) ):
            for nt in range(ds_g.TIME.shape[0]):
                # datestr = pd.to_datetime(str(ds_gs2.time[nt].values)).strftime('%Y%m%d')
                datestr = pd.to_datetime(ds_g.TIME[nt].values).strftime('%Y%m%d')

                # Load WMOP
                file_date = glob(F"{path_simulation}*{datestr}*avg*")[0]
                ds_date = xr.open_dataset(file_date)

                nlon = np.argmin(np.abs(ds_date.lon_rho[0,:].values - ds_g.LONGITUDE[nt].values))
                nlat = np.argmin(np.abs(ds_date.lat_rho[:,0].values - ds_g.LATITUDE[nt].values))

                # z_obs = depth_cmems # np.arange(0,800)
                z_obs = pres[nt,:]
                z_roms = zeta[:,nlat,nlon]
                z_roms = -z_roms

                # Interpolate
                ft = interp1d(z_roms, ds_date.temp[0,:,nlat,nlon].values, fill_value="extrapolate")
                t_interp[nt,:] = ft(z_obs)

                fs = interp1d(z_roms, ds_date.salt[0,:,nlat,nlon].values, fill_value="extrapolate")
                s_interp[nt,:] = fs(z_obs)

            mask_gli = salt / salt

            t_interp = t_interp * mask_gli
            s_interp = s_interp * mask_gli
            # d_interp[sim] = sw.dens0(s_interp[sim], t_interp[sim])

            t_1d[sim] = t_interp[ii]
            s_1d[sim] = s_interp[ii]

        temp = temp[ii]
        salt = salt[ii]
        pres = pres[ii]

        longitude = longitude[ii.flatten()]
        latitude = latitude[ii.flatten()]
        time = time[ii.flatten()]


        all_obs = {}

        all_obs['T CR'] = t_1d['CR']
        all_obs['T noGLI'] = t_1d['noGLI']
        all_obs['T GLI'] = t_1d['GLI']
        all_obs['S CR'] = t_1d['CR']
        all_obs['S noGLI'] = t_1d['noGLI']
        all_obs['S GLI'] = t_1d['GLI']


        df = pd.DataFrame(data=all_obs)

        df['T obs'] = temp
        df['S obs'] = salt
        df['time'] = time
        df['depth'] = pres
        df['longitude'] = longitude
        df['latitude'] = latitude
        
        df['institution'] = ds_g.institution
        df['platform_code'] = ds_g.platform_code
        df['platform_name'] = ds_g.platform_name

        if df_total is None:
            df_total = df
        else:
            df_total = pd.concat([df_total, df], ignore_index=True)
            
print('DONE!!!')

df_total.to_csv('socib_glider_opbservations_in_cmems_and_model equivalents_2017.txt')

df_total2 = df_total.copy()
df_total2 = df_total.set_index('time')

ds_total = xr.Dataset.from_dataframe(df_total2)

ds_total.to_netcdf('/LOCALDATA/Data/Observations/Eurosea_WP4_SOCIB_reanalysis_all-glider_observations_and_wmop_equivalents_v2.nc')



