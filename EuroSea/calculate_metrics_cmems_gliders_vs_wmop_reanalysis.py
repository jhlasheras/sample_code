import sys
sys.path.append('/home/jhernandez/Work/python_notebooks/EurSea/WP4/')
from glider_interpolation_plotting_tools import *
import os

# Glider files path
path_glider_cmems = '/home/modelling/data/Data/Observations/GLIDERS/CMEMS_EuroSea_T4.2/monthly/'
files_glider = glob(F'{path_glider_cmems}*')


simulations = {"CR": F"//mnt/vinson/DATA/modelling/WMOP_REANALYSIS/Outputs/EUROSEA_T4_FreeRun/forecast_scratch/",
               "noGLI": F"/mnt/vinson/DATA/modelling/WMOP_REANALYSIS/Outputs/EUROSEA_T4_noGLIDERS/forecast_scratch/",
               "GLI": F"/mnt/vinson/DATA/modelling/WMOP_REANALYSIS/Outputs/EUROSEA_T4_GLIDERS/forecast_scratch/"
              }

path_simulations = simulations['CR']

import pyroms
from scipy.interpolate import interp1d

file_date = glob(F"{simulations['CR']}*avg*")[0]

grd = pyroms.grid.get_ROMS_grid('grid', hist_file=file_date, grid_file=file_date)
zeta = grd.vgrid.z_r[:]

for sim in simulations:

    path_simulations = simulations[sim]

    df = pd.DataFrame(columns=['platform_code', 'platform_name', 'lon_min', 'lon_max', 'lat_min', 'lat_max',
                          'Corr T', 'RMSD T', 'Bias T', 'Corr S', 'RMSD S', 'Bias S'])

    for file in files_glider:

        if 'MO_PR' in file:
            continue

        temp_interp_cmems, salt_interp_cmems, dens_interp_cmems, dd = get_cmems_glider_regular(file)

        ds_g = xr.open_dataset(file)
        # Glider platform data
        tmax = pd.to_datetime( ds_g.TIME.max().values )
        tmin = pd.to_datetime( ds_g.TIME.min().values )
        lat_min = ds_g.LATITUDE.min().values
        lat_max = ds_g.LATITUDE.max().values
        lon_min = ds_g.LONGITUDE.min().values
        lon_max = ds_g.LONGITUDE.max().values

        platform_code = ds_g.platform_code
        platform_name = ds_g.platform_name

        institution = ds_g.institution

        temp_interp_wmop, salt_interp_wmop, dens_interp_wmop = interpolate_wmop_to_cmems_gliders(file, path_simulations, temp_interp_cmems, salt_interp_cmems, dd, zeta)

        #temp_interp_wmop[100:,:] = np.nan
        #salt_interp_wmop[100:,:] = np.nan


        ni = (~np.isnan(temp_interp_wmop)) & (~np.isnan(temp_interp_cmems))

        # Metrics Temperature
        corr_temp = np.corrcoef(temp_interp_wmop[ni], temp_interp_cmems[ni])[0][1]
        rmsd_temp = np.sqrt( np.nanmean( (temp_interp_wmop[ni] - temp_interp_cmems[ni])**2) )
        bias_temp = np.nanmean( (temp_interp_wmop[ni] - temp_interp_cmems[ni]))
        # metrics Salinity
        corr_salt = np.corrcoef(salt_interp_wmop[ni], salt_interp_cmems[ni])[0][1]
        rmsd_salt = np.sqrt( np.nanmean( (salt_interp_wmop[ni] - salt_interp_cmems[ni])**2) )
        bias_salt = np.nanmean( (salt_interp_wmop[ni] - salt_interp_cmems[ni]))

        # Insert Dict to the dataframe using DataFrame.append()
        new_row = {'platform_code': platform_code, 'platform_name': platform_name, 'institution': institution,
                   'lon_min': lon_min, 'lon_max': lon_max, 'lat_min': lat_min, 'lat_max': lat_max,
                   'Corr T': corr_temp, 'RMSD T': rmsd_temp, 'Bias T': bias_temp,
                   'Corr S': corr_salt, 'RMSD S': rmsd_salt, 'Bias S': bias_salt}

        df = df.append(new_row, ignore_index=True)

    df = df.dropna()

    df.to_csv(F'metrics_glider_cmems_vs_wmop_{sim}.txt')
    df.to_excel(F'metrics_glider_cmems_vs_wmop_{sim}.xlsx')


