#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 11 15:53:36 2022

@author: jhernandez
"""
import sys
sys.path.append('/home/jhernandez/Work/python_notebooks/EurSea/WP4/')
from glider_interpolation_plotting_tools import *

# Glider files path
path_glider_cmems = '/home/modelling/data/Data/Observations/GLIDERS/CMEMS_EuroSea_T4.2/monthly/'
files_glider = glob(F'{path_glider_cmems}*')


simulations = {"CR": F"//mnt/vinson/DATA/modelling/WMOP_REANALYSIS/Outputs/EUROSEA_T4_FreeRun/forecast_scratch/",
               "noGLI": F"/mnt/vinson/DATA/modelling/WMOP_REANALYSIS/Outputs/EUROSEA_T4_noGLIDERS/forecast_scratch/",
               "GLI": F"/mnt/vinson/DATA/modelling/WMOP_REANALYSIS/Outputs/EUROSEA_T4_GLIDERS/forecast_scratch/"
              }

path_simulations = simulations['CR']

import pyroms
from scipy.interpolate import interp1d

file_date = glob(F"{simulations['CR']}*avg*")[0]

grd = pyroms.grid.get_ROMS_grid('grid', hist_file=file_date, grid_file=file_date)
zeta = grd.vgrid.z_r[:]

sim = 'CR'
path_simulations = simulations[sim]

for i, f in enumerate(files_glider):
    
#    temp_interp2, salt_interp2, dens_interp2, dd = get_cmems_glider_regular(f)
#    fig = plot_cmems_glider_transect(f, temp_interp2, salt_interp2, dens_interp2, dd, plot_map=True, title='profiles gliders CMEMS')
#    
    glider_label = f.split('/')[-1][9:-3]
#    
#    fig.savefig(F"/LOCALDATA/Plots/EuroSea/WP4/sections/CMEMS_mission_{i}_glider-{glider_label}",
#            dpi=250, transparent=False, facecolor="w")
    
    
    
    temp_interp2, salt_interp2, dens_interp2, dd = get_cmems_glider_regular(f)
    t_wmop, s_wmop, d_wmop = interpolate_wmop_to_cmems_gliders(f, path_simulations, temp_interp2, salt_interp2, dd, zeta)
    fig = plot_cmems_glider_transect(f, t_wmop - temp_interp2, s_wmop - salt_interp2, d_wmop - dens_interp2, dd, plot_dif=True)
    
    
    savedir = F"/LOCALDATA/Plots/EuroSea/WP4/sections/{sim}/"
    
#    if not os.path.exists(savedir):
#        os.makedirs(savedir)
        
    fig.savefig(F"{savedir}/CMEMS_mission_{i}_glider-{glider_label}.png",
            dpi=250, transparent=False, facecolor="w")
    fig.close()