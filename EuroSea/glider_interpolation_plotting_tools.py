#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on %(date)s

@author: %(username)s
"""
import cartopy.crs as ccrs
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import cartopy.feature as cfeature

import sys
sys.path.append('/home/jhernandez/Work//')
sys.path.append('/home/jhernandez/Work/python_tools/')

from defaults import *
from tqdm import tqdm

from scipy.interpolate import griddata
from scipy import interpolate
from scipy.interpolate import interp1d

import seawater as sw


def get_cmems_glider_regular(file):
    
    ds_g = xr.open_dataset(file)
    
    # Get Glider data
    pres = ds_g.PRES.values.flatten()
    temp = ds_g.TEMP.values.flatten()
    temp_qc = ds_g.TEMP_QC.values.flatten()
    salt = ds_g.PSAL.values.flatten()
    salt_qc = ds_g.PSAL_QC.values.flatten()

    nz = ds_g.DEPTH.shape[0]
    time = np.tile(ds_g.TIME.values,(nz,1)).T.flatten()
    time = np.repeat(ds_g.TIME.values, nz)

    ii = (~np.isnan(temp)) & (~np.isnan(salt)) & (temp_qc==1) & (salt_qc==1) & (~np.isnan(pres)) 
    
    if True not in ii:
        print(F'Alert!! No data with QC==1! for {file}')
        ii = (~np.isnan(temp)) & (~np.isnan(salt)) & (temp_qc==1) & (~np.isnan(pres)) 

    # Calculate density
    dens = sw.pden(salt[ii], temp[ii], pres[ii])
    # Calculate potential temperature
    ptemp = sw.ptmp(salt[ii], temp[ii], pres[ii])
    
    # Create regular Grid
    dd = np.arange(0,800)
    tt = np.arange(ds_g.TIME.shape[0])
    xx, yy = np.meshgrid(tt, dd)
    xx = xx.flatten()
    yy = yy.flatten()

    # Set grid Depth, Time from Glider observations as tuple of arrays
    lonlat = np.array([xx,yy]).T
    points = np.array([np.repeat(tt, nz)[ii], pres[ii]]).T
    # Grid Data
    # temp_interp = griddata(points, temp[ii], lonlat, method='nearest')
    temp_interp = griddata(points, ptemp, lonlat, method='nearest')
    salt_interp = griddata(points, salt[ii], lonlat, method='nearest')
    dens_interp = griddata(points, dens, lonlat, method='nearest')

    # Reshape to regular
    temp_interp2 = np.reshape(temp_interp, (800, ds_g.TIME.shape[0]))
    salt_interp2 = np.reshape(salt_interp, (800, ds_g.TIME.shape[0]))
    dens_interp2 = np.reshape(dens_interp, (800, ds_g.TIME.shape[0]))
    dens_interpc = dens_interp2.copy()

    # Put NaNs where no data available
    for i in range(ds_g.TIME.shape[0]):
        max_level = int(ds_g.PRES[i,:].max().values.round())
        min_level = int(ds_g.PRES[i,:].min().values.round())
        # if max_level > 800:
        temp_interp2[max_level:,i] = np.nan; temp_interp2[:min_level+1,i] = np.nan
        salt_interp2[max_level:,i] = np.nan; salt_interp2[:min_level+1,i] = np.nan
        dens_interp2[max_level:,i] = np.nan; dens_interp2[:min_level+1,i] = np.nan

    return temp_interp2, salt_interp2, dens_interp2, dd


def plot_cmems_glider_transect(file, temp_interp, salt_interp, dens_interp, dd, 
                               plot_dif=False, plot_map=True, title=''):
    
    ds = xr.open_dataset(file)
    X, Y = np.meshgrid(ds.TIME.values, dd)

    fig = plt.figure(figsize=(15,6))
    plt.subplot(1,3,1)
    if plot_dif == True:
        vmax = np.nanmax(temp_interp)
        plt.pcolormesh(X, -Y, temp_interp, cmap='RdBu', vmin=-vmax, vmax=vmax)
    else:    
        plt.pcolormesh(X, -Y, temp_interp, cmap='Spectral_r')#, vmin=14, vmax=27.3)
    plt.colorbar()
    plt.xticks(rotation=45)
    plt.xlim((X.min(), X.max()))
    # plt.ylim((-100,0))
    plt.xticks(rotation=45)
    plt.title(F"Temperature {title}", fontsize=14, fontweight='bold')

    plt.subplot(1,3,2)

    if plot_dif == True:
        vmax = np.nanmax(salt_interp)
        plt.pcolormesh(X, -Y, salt_interp, cmap='RdBu', vmin=-vmax, vmax=vmax)
    else:
        plt.pcolormesh(X, -Y, salt_interp)#, vmin=38, vmax=39.3)
    plt.colorbar()
    plt.xticks(rotation=45)
    plt.title(F"Salinity {title}", fontsize=14, fontweight='bold')
    
    
    plt.subplot(1,3,3)

    if plot_dif == True:
        vmax = np.nanmax(dens_interp)
        plt.pcolormesh(X, -Y, dens_interp, cmap='RdBu', vmin=-vmax, vmax=vmax)
    else:
        plt.pcolormesh(X, -Y, dens_interp, cmap='RdYlBu_r')#, vmin=38, vmax=39.3)
    plt.colorbar()
    # plt.ylim((-100,0))
    plt.xticks(rotation=45)
    plt.title(F"Density {title}", fontsize=14, fontweight='bold')
    
    if plot_map==True:
        # this is an inset axes over the main axes
        ax2 = plt.axes([.68, .15, .23, .23], projection=ccrs.Mercator())#facecolor='y')
        lim_map = [min(ds.LONGITUDE.values)-2, max(ds.LONGITUDE.values)+2, min(ds.LATITUDE.values)-2, max(ds.LATITUDE.values)+2]
        ax2.set_extent(lim_map)#[-6,10,35,45])
        ax2.coastlines('10m')
        # configure nice plot
        gl = ax2.gridlines(crs=ccrs.PlateCarree(), draw_labels=False, linewidth=0)
        ax2.scatter(ds.LONGITUDE, ds.LATITUDE, 5, ds.TIME, transform=gl.crs)
       

    plt.show()
    
    return fig 


def interpolate_wmop_to_cmems_gliders(file, path_simulation, temp_interp_cmems, salt_interp_cmems, depth_cmems, zeta):
    
    ds_g = xr.open_dataset(file)

    # Mask for points at which there are Glider Obs
    mask_gli = (temp_interp_cmems / temp_interp_cmems)

    tmin_gli = pd.to_datetime(ds_g.TIME.min().values).strftime('%Y%m%d')
    tmax_gli = pd.to_datetime(ds_g.TIME.max().values).strftime('%Y%m%d')


    print(F"Calculating RMSD for SOCIB Glider in Ibiza Channel between {tmin_gli} and {tmax_gli}")


    t_interp = np.empty_like( temp_interp_cmems )
    s_interp = np.empty_like( salt_interp_cmems )

    for nt in tqdm(range(ds_g.TIME.shape[0]) ):
        # datestr = pd.to_datetime(str(ds_gs2.time[nt].values)).strftime('%Y%m%d')
        datestr = pd.to_datetime(ds_g.TIME[4].values).strftime('%Y%m%d')

        # Load WMOP
        file_date = glob(F"{path_simulation}*{datestr}*avg*")[0]
        ds_date = xr.open_dataset(file_date)

        nlon = np.argmin(np.abs(ds_date.lon_rho[0,:].values - ds_g.LONGITUDE[nt].values))
        nlat = np.argmin(np.abs(ds_date.lat_rho[:,0].values - ds_g.LATITUDE[nt].values))

        z_obs = depth_cmems # np.arange(0,800)
        z_roms = zeta[:,nlat,nlon]
        z_roms = -z_roms

        # Interpolate
        ft = interp1d(z_roms, ds_date.temp[0,:,nlat,nlon].values, fill_value="extrapolate")
        t_interp[:,nt] = ft(z_obs)

        fs = interp1d(z_roms, ds_date.salt[0,:,nlat,nlon].values, fill_value="extrapolate")
        s_interp[:,nt] = fs(z_obs)

    t_interp = t_interp * mask_gli
    s_interp = s_interp * mask_gli
    d_interp = sw.dens0(s_interp, t_interp)

    
    

#    rmsd_temp = (np.sqrt( np.nanmean( (t_interp - temp_interp_cmems)**2 )))
#    rmsd_salt = (np.sqrt( np.nanmean( (s_interp - salt_interp_cmems)**2 )))
#
#    print(F"\tTemp RMSD = {rmsd_temp:0.3f}")
#    print(F"\tSalt RMSD = {rmsd_salt:0.3f}")
#    print("")

    return t_interp, s_interp, d_interp