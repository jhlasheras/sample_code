#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 21 13:12:14 2018

@author: jhernandez
"""

import numpy as np
from scipy import interpolate
import xarray as xr
import netCDF4

def complex_corr(u1,v1, u2, v2):
    
    module_x1 = np.sqrt(np.square(u1) + np.square(v1))
    module_x2 = np.sqrt(np.square(u2) + np.square(v2))
    
    rho = (u1*u2 + v1*v2) / (module_x1*module_x2)
    
    prod_esc = u1*u2 + v1*v2
    phase_rad = np.arccos(prod_esc/(module_x1*module_x2))
    phase_deg = phase_rad*180/np.pi
    
    
    sign = (u1*v2 - u2*v1) / abs(u1*v2 - u2*v1)
    phase_deg = phase_deg*sign
    
    #phase2 = np.arctan((u1*v2 - v1*u2) / (u1*u2 + v1*v2))
    #phase2 = phase2*180/np.pi
    
    return rho, phase_deg



def find_nearest(array, value):
    ''' Find nearest value is an array '''
    idx = (np.abs(array-value)).argmin()
    return idx



def wmop_to_radar(wmop_file, radar_lon, radar_lat, radar_mask):
    
    ''' Interpolates WMOP fields to HFR measurement points '''
    
    # Import WMOP variables
    ds = netCDF4.Dataset(wmop_file)
 
    lat_wmop = ds.variables['lat_rho'][:]
    lon_wmop = ds.variables['lon_rho'][:]
    
    
    
    # Find correspondant values of WMOP fields in HFR area 
    lonmin_radar = np.min(radar_lon)
    lonmax_radar = np.max(radar_lon)
    idx_lonmin = find_nearest(lon_wmop[0,:], lonmin_radar)
    idx_lonmax = find_nearest(lon_wmop[0,:], lonmax_radar)
    
    latmin_radar = np.min(radar_lat)
    latmax_radar = np.max(radar_lat)
    idx_latmin = find_nearest(lat_wmop[:,0], latmin_radar)
    idx_latmax = find_nearest(lat_wmop[:,0], latmax_radar)
    
    # Extract velocity fields in HFR area
    u_wmop = ds.variables['u'][0, -1, idx_latmin:idx_latmax, idx_lonmin:idx_lonmax]
    v_wmop = ds.variables['v'][0, -1, idx_latmin:idx_latmax, idx_lonmin:idx_lonmax]
    lat_wmop = lat_wmop[idx_latmin:idx_latmax, idx_lonmin:idx_lonmax]
    lon_wmop = lon_wmop[idx_latmin:idx_latmax, idx_lonmin:idx_lonmax]
    
    # Interpolate to HFR grid
    x,y = np.meshgrid(radar_lon, radar_lat)
    u_grid = interpolate.griddata((lon_wmop.flatten(),lat_wmop.flatten()),u_wmop.flatten(),(x,y))
    v_grid = interpolate.griddata((lon_wmop.flatten(),lat_wmop.flatten()),v_wmop.flatten(),(x,y))
    u_grid[u_grid>2] = np.NaN
    v_grid[v_grid>2] = np.NaN
    
    return u_grid, v_grid



