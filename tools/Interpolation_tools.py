

import scipy.interpolate as interp

def interp_wmop_to_hfr(ds, hfr):
    
    # Interp U
    u = ds.u[0,-1,:,:].values
    u[np.isnan(u)] = 100

    fu = interp.interp2d(ds.lon_u[1,:].values, ds.lat_u[:,1].values, u, kind='linear')
    u_interp = fu(hfr['lon'], hfr['lat'])
    u_interp[u_interp>1] = np.nan
    
    if 'u_radar' in hfr.keys():
        u_interp = np.ma.masked_array(u_interp, mask=hfr['u_radar'].mask) # mask
    
    # Interp V
    v = ds.v[0,-1,:,:].values
    v[np.isnan(v)] = 100

    fv = interp.interp2d(ds.lon_v[1,:].values, ds.lat_v[:,1].values, v, kind='linear')
    v_interp = fv(hfr['lon'], hfr['lat'])
    v_interp[v_interp>1] = np.nan
    
    if 'u_radar' in hfr.keys():
        v_interp = np.ma.masked_array(v_interp, mask=hfr['v_radar'].mask) # mask
    
    return u_interp, v_interp



def interp_wmop_to_radial(ds, df):
    
    # Interp U
    u = ds.u[0,-1,:,:].values
    u[np.isnan(u)] = 100

    fu = interp.interp2d(ds.lon_u[1,:].values, ds.lat_u[:,1].values, u, kind='linear')
    u_interp = [fu(df['lon'][i], df['lat'][i]).tolist()[0] for i in range(len(df['lon']))]
    u_interp = np.array(u_interp)
    u_interp[u_interp>1.0] = np.nan
       
    # Interp V
    v = ds.v[0,-1,:,:].values
    v[np.isnan(v)] = 100

    fv = interp.interp2d(ds.lon_v[1,:].values, ds.lat_v[:,1].values, v, kind='linear')
    v_interp = [fv(df['lon'][i], df['lat'][i]).tolist()[0] for i in range(len(df['lon']))]
    v_interp = np.array(v_interp)
    v_interp[v_interp>1.0] = np.nan
    
    return u_interp, v_interp




def project_to_radial(u, v, bearing):
    """Project to radial observation given the u and v components of an observed velocity
    and the bearing with the antenna at the observation point"""
    
    module = np.sqrt(u**2 + v**2)
    alpha = np.arctan2(v,u)
               
    bearing = bearing*np.pi/180      
    
    theta = bearing - alpha
    radial = module * np.cos(theta)
    radial[radial > 0.3] = np.nan
    radial[radial < -0.3] = np.nan
    
    return radial
