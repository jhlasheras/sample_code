
import pickle

def set_folder( folder):    
    if folder:
        folder = folder
    else:
        folder = '/home/jhernandez/Work/data/'
    return folder
    
def save_obj(obj, name, folder = None ):    
    folder = set_folder( folder)    
    with open(folder + name + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

def load_obj(name, folder=None ):    
    folder = set_folder( folder)
    with open(folder + name + '.pkl', 'rb') as f:
        return pickle.load(f)
