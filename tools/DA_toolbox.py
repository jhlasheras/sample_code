
from datetime import datetime
from datetime import timedelta
import numpy as np
import xarray as xr
import glob
import pandas as pd
import matplotlib.pyplot as plt
import wget
import os

from pathlib import Path
import xarray as xr
from scipy import signal


def get_altimetry_data( strdate,  product, timewindow=3, limits = [-6, 9.2, 35, 44.5]):

    time_max = datetime.strptime(strdate,'%Y%m%d')

    dates = [time_max - timedelta(days=d) for d in range(timewindow)]
    dates.sort()

    products=['nrt_along-track_sla','nrt_along-track_sla_assim','nrt_map_sla','rep_along-track_sla','rep_along-track_sla_unfiltered','rep_map_sla']
    altinames=['al','j2','h2','c2','e1','e2','en','enn','g2','j1','j1g','j1n','tp','tpn','j3','s3a','twosat','allsat','unknown']

    print('\n Loading SLA observations for {0}. Time window = {1} days\n'.format(time_max.strftime('%d-%b-%Y'), timewindow))

    if product in products:
        dirname_alti = '/home/modelling/data/Data/Observations/ALTIMETRY/'
        new = 1
    else:
        dirname_alti = '/home/modelling/data/Data/Observations/ALTIMETRY/OLD_AVISO/'
        new = 0

    elenco = []

    if 'map' in product:
        path = '{0}{1}/{2}/*{3}*.nc'.format(dirname_alti, product, time_max.strftime('%Y/%m'), strdate)
        filename = glob.glob(path)[0]
        # Load DataSet
        ds = xr.open_dataset(filename)
        if 'lon' in list(ds.dims):
            lon = 'lon'; lat = 'lat'
        else:
            lon = 'longitude'; lat = 'latitude'

        # Correct longitude
        #if 'rep' in product:
         #   ds[lon].values = ds[lon].values-360
        # Select observations within the spatial and temporal window requests
        ds = ds.where((ds[lon] > limits[0]) & (ds[lon] < limits[1]) & (ds[lat] > limits[2]) & (ds[lat] < limits[3]), drop=True)
        
        ds_total = ds
    
    else:

        for date in dates:

            if new == 1:
                path = '{0}{1}/{2}/*.nc'.format(dirname_alti, product, date.strftime('%Y/%m'))
            else:
                path = '{0}{1}/{2}/*.nc'.format(dirname_alti, product, date.strftime('%Y'))

            satellite_name = {}

            elenco = elenco + glob.glob(path)
        
        elenco = list(set(elenco))
        
        for filename in elenco:

            flag = [a in filename for a in altinames]

            if not True in flag:
                break

            # Load DataSet
            ds = xr.open_dataset(filename)

            # Correct longitude
            ds['longitude'][ds['longitude']>180] = ds['longitude'][ds['longitude']>180]-360

            # Select observations within the spatial and temporal window requests
            #ds = ds.where((ds.longitude > limits[0]) & (ds.longitude < limits[1]))
            ds = ds.where((ds['longitude'] > limits[0]) & (ds['longitude'] < limits[1]) & (ds['latitude'] > limits[2]) & (ds['latitude'] < limits[3]), drop=True)
            ds = ds.where( ds['time'].loc[dates[0]:dates[-1]])
            ds = ds.dropna(dim='time')

            # Add platform name as new variable
            ds['sou'] = xr.Variable(data = [ds.platform]*len(ds.time), dims = 'time')

            # Concatenate observations
            if 'ds_total' in locals():
                ds_total = xr.concat([ds_total, ds], dim='time')
            else:
                ds_total = ds

            # Count number of observations per platform
            if ds.platform in satellite_name.keys():
                satellite_name[ds.platform].append(len(ds.time))
            else:
                satellite_name[ds.platform] = [len(ds.time)]


        for ii in satellite_name:
            print('     {0}  Number obs = {1}'.format(ii, sum(satellite_name[ii])))

        total_obs = sum([sum(satellite_name[ii]) for ii in satellite_name])
        print('\n   Total number of observations = {0}'.format(total_obs)) 

    
    return ds_total
        


def get_CMEMS_OISST(strdate, product='L3S_UHR', limits=[-6, 9.2, 35, 44.5], sst_interpolated=True, smooth=False, resolution=10):
    
    """ Get SST observations from CMEMS server. Download observations for a selected date and selects the corresponding data within the selected bounding box. Resolution can be selected and interpolation and smoothing flags may be activated 
    
    Inputs: strdate (str), product(str), limits (list), sst_interpolated (boolean), smooth (boolean), resolution (int)
    Output: DataSet containing the SST observations, according to input demands
    
    get_L3S_OISST(strdate, product='L3S_UHR', limits=[-6, 9.2, 35, 44.5], sst_interpolated=True, smooth=False, resolution=10)
    
    """
    

    date = datetime.strptime(strdate,'%Y%m%d')


    print(F"    Downloading {product} SST product for {date.strftime('%Y-%m-%d')} from CMEMS ftp server ...")

    path = {
        'L3S_HR': ['000000-GOS-L3S_GHRSST-SSTsubskin-night_SST_HR_NRT-MED-v02.0-fv01.0.nc',
                  'SST_MED_SST_L3S_NRT_OBSERVATIONS_010_012',
                   'SST_MED_SST_L3S_NRT_OBSERVATIONS_010_012_a'], 
        'L3S_UHR': ['000000-GOS-L3S_GHRSST-SSTsubskin-night_SST_UHR_NRT-MED-v02.0-fv01.0.nc',
                   'SST_MED_SST_L3S_NRT_OBSERVATIONS_010_012',
                   'SST_MED_SST_L3S_NRT_OBSERVATIONS_010_012_b'],
        'L4_HR': ['000000-GOS-L4_GHRSST-SSTfnd-OISST_HR_NRT-MED-v02.0-fv02.0.nc',
                 'SST_MED_SST_L4_NRT_OBSERVATIONS_010_004',
                 'SST_MED_SST_L4_NRT_OBSERVATIONS_010_004_a_V2'],
        'L4_UHR': ['000000-GOS-L4_GHRSST-SSTfnd-OISST_UHR_NRT-MED-v02.0-fv02.0.nc',
                  'SST_MED_SST_L4_NRT_OBSERVATIONS_010_004',
                 'SST_MED_SST_L4_NRT_OBSERVATIONS_010_004_c_V2']
    }

    # define file name
    fname = strdate + path[product][0]
    sst_file = 'ftp://nrt.cmems-du.eu/Core/{0}/{1}/{2}/{3}'.format(path[product][1], path[product][2], date.strftime('%Y/%m'), fname)

    # download file
    os.system('wget --user=mjuza --password=yBDpEgBR {0}'.format(sst_file))

    if os.path.isfile(fname):

        ds = xr.open_dataset(fname)
        # subset according to bounding box
        ds = ds.sel(lon=slice(limits[0], limits[1])).sel(lat=slice(limits[2], limits[3]))

        if sst_interpolated:
            nb = int(np.floor(resolution/2))

            # Smoth field
            if smooth==True:

                kernel = np.ones((nb,nb))
                sst = ds.analysed_sst[0,:,:].values
                sst = signal.convolve2d(sst, kernel, boundary='symm', mode='same')
                ds.analysed_sst[0,:,:] = sst

            ds = ds.isel(lon=np.arange(0, ds.lon.shape[0], nb)).isel(lat=np.arange(0, ds.lat.shape[0], nb))
    
    # Remove file
    os.system('rm -f {0}'.format(fname))
    #os.remove('{0}'.format(fname))     
    return ds







def get_GHRSST_JPL_SST_data(strdate, limits=[-6, 9.2, 35, 44.5], strraw='interp', resolution=10):

    
    mydate = datetime.strptime(strdate,'%Y%m%d')

    if 'raw' in strraw:
        sst_interpolated=0
    else:
        sst_interpolated=1

    dirname = '/home/modelling/data/Data/Observations/GHRSST-JPL/'
    #dirname_temp = '/data/jhernandez/Observations/GHRSST-JPL_temp/'  # to be removed in cluster version
    dirname_temp = '/home/jhernandez/Observations/GHRSST-JPL_temp/'

    file_jpl = '{0}{1}-JPL-L4UHfnd-WMED-v01-fv04-MUR.nc'.format(dirname, mydate.strftime('%Y/%Y%m%d'))

    if Path(file_jpl + '.gz').is_file():

        os.system('cp -f ' + file_jpl + '.gz ' + dirname_temp)  # to be removed in cluster version
        file_jpl= dirname_temp + strdate + '-JPL-L4UHfnd-WMED-v01-fv04-MUR.nc'  # to be removed in cluster version

        os.system('gunzip -f ' + file_jpl + '.gz')

    if Path(file_jpl).is_file():
        ds = xr.open_dataset(file_jpl)         

        # If need for interpolation get a bigger bounding box
        add_lon = sst_interpolated*1.1*resolution/85
        add_lat = sst_interpolated*1.1*resolution/111

        ds = ds.where((ds.lon>limits[0]-add_lon) & (ds.lon<limits[1]+add_lon) & (ds.lat>limits[2]-add_lat) & (ds.lat<limits[3]+add_lat), drop=True)

        if sst_interpolated:

            # Apply 2D smoothing before interpolation to ROMS grid (NOT DONE here!)
            # SST GHRSST-JPL has 0.86km resolution while ROMS grid has a 1.88 to 2.17km resolution

             # Smoth field
            nb = int(np.floor(resolution))
            kernel = np.ones((nb,nb))/(nb*nb)

            sst = ds['analysed_sst'][0,:,:].values
            sst = signal.convolve2d(sst-273, kernel, boundary='symm', mode='same')
            sst_error = ds['analysis_error'][0,:,:].values
            sst_error = signal.convolve2d(sst_error, kernel, boundary='symm', mode='same')

            ds['analysed_sst'][0,:,:] = sst
            ds = ds.isel(lon=np.arange(0, ds.lon.shape[0], nb)).isel(lat=np.arange(0, ds.lat.shape[0], nb))
            
        else:
            ds['analysed_sst'][0,:,:] = ds['analysed_sst'][0,:,:].values - 273
            

    os.system('rm -f ' + file_jpl + ' ' + file_jpl + '.gz')

    print('GHRSST JPL-MUR L4 observations extracted for {0}'.format(mydate.strftime('%d-%m-%Y')))
    
    return ds

        
