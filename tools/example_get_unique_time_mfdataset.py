def get_unique_times_index(data):
    total_times = data.dims['ocean_time']
    times = data.ocean_time.values
    set_times = set()
    unique_index = []
    for t in reversed(range(total_times)):
        if times[t] not in set_times:
            set_times.add(times[t])
            unique_index.append(t)
    unique_index = list(reversed(unique_index))
    
    return unique_index
  
wmop_files = []
start = pendulum.datetime(2019, 6, 1)
end = pendulum.datetime(2019, 12, 31)
period = pendulum.period(start, end)
for date in tqdm(period.range('days', 1)):
    wmop_file = F"{wmop_dir}/roms_WMOP_FORECAST_{date.year}{date.month:02}{date.day:02}_his.nc"
    try:
        if Path(wmop_file).is_file():
            wmop_files.append(wmop_file)
    except:
        print(F"NOT FOUND: {wmop_file}")
        pass
wmop_data = xr.open_mfdataset(wmop_files)
unique = get_unique_times_index(wmop_data)
wmop_data = wmop_data.isel(ocean_time=unique)
