import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from glob import glob
from datetime import datetime 
from datetime import timedelta

import scipy.interpolate as interp


import warnings
warnings.filterwarnings('ignore')

import sys
sys.path.append('/home/jhernandez/python_scripts/Tools/')
import tools
#import pyroms
import colormaps
import myio
