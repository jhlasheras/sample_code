# coding: utf-8

# In[ ]:


import cartopy.crs as ccrs
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import cartopy.feature as cfeature
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import matplotlib
import pyroms
import pandas as pd
import xarray as xr
import numpy as np
import numpy.matlib
import seawater as sw

from IPython.core.display import display, HTML

from DA_toolbox import *
from get_hfr_data import *



##---------------------- PLOT FUNCTIONS ---------------------------------

def make_map(limits=[-6, 9, 34.8, 45], subplot_dim=None, title=None, continents=False, landres='10m',
             fsize=12, xloc=None, yloc=None):
    """ Plot Basemap 
            ax, gl = make_map( limits, continents, subplot_dim, landres)
            To plot any data over the map use when plotting  'transform=gl.crs'  """
    
    if subplot_dim==None:
        ax = plt.subplot(111,projection=ccrs.Mercator())
        
    else:
        if type(subplot_dim)==int:
            subplot_dim = [int(s) for s in str(subplot_dim)]
            
        ax = plt.subplot(subplot_dim[0], subplot_dim[1], subplot_dim[2], projection=ccrs.Mercator())
        
    ax.set_extent(limits)

    # configure nice plot
    gl = ax.gridlines(crs=ccrs.PlateCarree(), draw_labels=False, linewidth=0)

    gl.xlabels_bottom = True
    gl.ylabels_left = True
    
    if xloc:
        gl.xlocator = mticker.FixedLocator(xloc)
    if yloc:
        gl.ylocator = mticker.FixedLocator(yloc)
    
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER
    gl.xlabel_style = {'size': fsize}
    gl.ylabel_style = {'size': fsize}
    

    # Add continents and coastline as a festure
    if continents==True:
        #ax.add_feature(cfeature.GSHHSFeature(scale='h',facecolor=[0.94,0.94,0.94]))
        #ax.add_feature(cfeature.NaturalEarthFeature(scale=10,facecolor=[0.94,0.94,0.94]))
        land = cfeature.NaturalEarthFeature('physical', 'land', landres,
                                            edgecolor='face',
                                            facecolor=[0.94,0.94,0.94])#cfeature.COLORS['land'])
        ax.add_feature(land)
        ax.coastlines(resolution=landres)
    else:
        ax.coastlines('10m')
        
    if title:
        plt.title(title, fontsize=fsize, fontweight='bold')
        
    
    return ax, gl

   
    

def get_bathimetry(database, 
                   limits=[-6.5,9.5,34.5,43.5]):

    """ Get bathymetry for selcted Database. 
        Databases available: Smith_Sandwell, ETOPO2, ETOPO1, GEBCO_1min, GEBCO_30sec, STRM30
        
        Inputs: database, limits
        Output: xarray Database with selcted bathimetry
        Eg: get_bathimetry('ETOPO1', limits=[-1.5, 2, 34.5, 35.5]
        
        """
    
    bathi = {'Smith_Sandwell': ['MED_smith_sandwell_v9_1.nc', 'lon', 'lat'],
             'ETOPO2': ['bathymetry_ETOPO2v2c_WMED.nc', 'x', 'y'],
             'ETOPO1': ['ETOPO1_Bed_g_gmt4.nc', 'x', 'y'],
             'GEBCO_1min': ['MED_GEBCO_1min.nc', 'lon', 'lat'],
             'GEBCO_30sec': ['MED_GEBCO_30sec.nc', 'lon', 'lat'],
             'STRM30': ['MED_srtm30plus_v4.nc', 'lon', 'lat']}
    
    dirname_bathy = '/mnt/waverider/data/Data/Observations/BATHYMETRY/'
    
    
    longitude = bathi[database][1]
    latitude = bathi[database][2]
    
    if database in bathi.keys():
        ds = xr.open_dataset(dirname_bathy + bathi[database][0])
        ds = ds.where((ds[longitude]>limits[0]) & (ds[longitude]<limits[1]) & (ds[latitude]>limits[2]) & (ds[latitude]<limits[3]), drop=True )
        
        return ds
    else:
        print ('No Bathymetry for database selected. \nTry one of the following: {0}'.format( list(bathi.keys())))
        return

    

def plot_differences_fields( ds1, ds2, variable, depth, grd, limits=[-6, 9, 34.8, 45], **kwargs):
    """For two diven xarray datasets plots differences for the 
    selected field at a certain depth
    Requires: pyroms grid, xarray datasets
    Inputs:
     - Two datasets: from which extract the fields to compare and plot
     - Variable and depth"""
    
    
    if len(ds1[variable].shape)==4:
        
        var1 = pyroms.tools.zslice( ds1[variable][0,:,:,:].values, depth, grd)[0]
        var2 = pyroms.tools.zslice( ds2[variable][0,:,:,:].values, depth, grd)[0]
        
    else:
        
        var1 = ds1[variable][0,:,:]
        var2 = ds2[variable][0,:,:]
    
      
    
    fig = plt.figure(figsize=(13,8))
    ax, gl = make_map(limits)
    plt.pcolormesh( ds1.lon_rho, ds1.lat_rho, var1-var2, transform=gl.crs, cmap='RdYlBu_r', **kwargs)
    plt.colorbar()
    
    plt.title('{0} difference at {1}m'.format(ds1[variable].long_name, depth), 
             fontsize=15, fontweight='bold')
    plt.show()
        
    return ax, gl, fig



##-------------------MANAGE OBSFILES FOR DA ---------------------


def read_obsfile(obs_input_file, radials='False'):
    
    names = ['var','source','year','month','day','hour','minute','lon','lat','depth','val','err','rep']
    if radials == True:
        names = ['var','source','year','month','day','hour','minute','lon','lat','depth','val','err','rep','bearing']
    
    # read observations
    df_obs = pd.read_csv(obs_input_file,
                      names=names,
                      header=None,
                      delimiter=' ')
    return df_obs


def write_obsfile(df_obs, obs_output_file):
    
    # round decimals
    decimals = pd.Series([6,6,6,6,6], index= df_obs.keys()[7:12])
    df_obs.round(decimals)

    # save observation DataFrame
    df_obs.to_csv(obs_output_file, header=None, sep=' ', index=False,  float_format='%.6f')
    
    return

def get_innovations(inn_file, obs_file):
    """Load innovation and observations files as
    pandas dataframes and merge them together
    
    Inputs: innovation file and observation file from DA analysis
    Output: Dataframe containing merged innovation and observation dataframes
    """
    
    # Load innovation and observations
    inn = read_inn_file(inn_file)
    obs = read_analysis_obsfile(obs_file)

    # Create index to merge through
    inn['merge_ind']=inn.index
    obs['merge_ind']=obs.index

    # Merge datadrames and drop repeated fields
    df = inn.merge(obs,how='outer', on='merge_ind')
    df = df.drop(['obs_lon', 'obs_lat', 'obs_depth', 'obs_value', 'err', 'rep', 'merge_ind', 'sync'], axis=1)
    
    # Sort columns 
    variables_list = [ 'source', 'lon', 'lat', 'depth', 'val',  'innovation', 'reg_coeff', 'increment',
       'analysis', 'background', 'bg_err_variance', 'obs_err', 'obs_noise','nb_sou',  'year',
       'month', 'day', 'hour', 'minute']
    df = df[variables_list]
    
    return df

def read_inn_file(inn_file):
    
    """Load innovation files as pandas dataframes and merge them together
    
    Inputs: innovation file from DA analysis
    Output: Dataframe containing innovation dataframe
    """
    
    inn = pd.read_csv(inn_file, delimiter=' ', header=None, skiprows=[0], names = ['obs_nb', 'obs_lon', 'obs_lat', 'obs_depth', 'obs_value', 'obs_err', 'obs_noise', 'innovation', 'reg_coeff', 'increment', 'analysis', 'background', 'bg_err_variance'], index_col=[0])
    
    return inn


def read_analysis_obsfile(obs_file):
    
    """Load observation files as pandas dataframes and merge them together
    
    Inputs: Observation file from DA analysis
    Output: Dataframe containing observation dataframe
    """
    obs = pd.read_csv(obs_file, delimiter=' ', header=None, index_col=[1], names=['sync', 'source', 'nb_sou', 'year', 'month', 'day', 'hour', 'minute', 'lon', 'lat', 'depth', 'val', 'err', 'rep'])
    
    return obs


##-----------------------------------------------------------------------

def get_geostrophic_velocities_from_ds(ds, ntime=0):
    """Calculate geostrophic currents from a sea surface
    height (SSH) field from a model DataSet (use xarray)
    Inputs: 
     - DataSet: loaded using xarray
     - ntime: datastep within the dataset
    """

    g = 9.81;
    omega = 7.292115 * 1e-5;
    deg2meters = 60 * 1852;

    latCellSize = (np.diff(ds.lat_rho[:,0]) * deg2meters)
    latCellSize = np.hstack((latCellSize,latCellSize[-1]))
    latCellSize = np.matlib.repmat(latCellSize,ds.lat_rho.shape[1],1).transpose()
    lonCellSize = (np.diff(ds.lon_rho[0,:]) * deg2meters);
    lonCellSize = np.hstack((lonCellSize,lonCellSize[-1]))
    lonCellSize = lonCellSize * np.cos(np.deg2rad(ds.lat_rho))

    fcoriolis = 2 * omega * np.sin(np.deg2rad(ds.lat_rho));
    factor = g / fcoriolis;

    u_geo = -factor*np.gradient(ds.zeta[ntime])[0]/lonCellSize
    v_geo = +factor*np.gradient(ds.zeta[ntime])[1]/latCellSize

    return u_geo,v_geo



##--------------------------ROMS UTILITIY FUNCTIONS-----------------------------
	
		
def v2rho_3d(var_v):
	d,N,M,L = var_v.shape
	var_rho = np.zeros((d,N,M+1,L))
	var_rho[0,:,1:M-1,:] = 0.5*(var_v[0,:,0:M-2,:]+var_v[0,:,1:M-1,:])
	var_rho[0,:,0,:] = var_rho[0,:,1,:]
	var_rho[0,:,M,:] = var_rho[0,:,M-1,:]
	#mask_rho = np.load('mask_rho.npy')
	#var_rho[:,:,mask_rho==0]=np.nan
	return (var_rho)	
	
def u2rho_3d(var_u):
	d,N,Mp,L = var_u.shape
	var_rho = np.zeros((d,N,Mp,L+1))
	var_rho[0,:,:,1:L-1] = 0.5*(var_u[0,:,:,0:L-2]+var_u[0,:,:,1:L-1]);
	var_rho[0,:,:,0] = var_rho[0,:,:,1];
	var_rho[0,:,:,L] = var_rho[0,:,:,L-1];
	#mask_rho = np.load('mask_rho.npy')
	#var_rho[:,:,mask_rho==0]=np.nan
	return(var_rho)


def v2rho_2d(var_v):
	M,L = var_v.shape
	var_rho = np.zeros((M+1,L))
	var_rho[1:M-1,:] = 0.5*(var_v[0:M-2,:]+var_v[1:M-1,:])
	var_rho[0,:] = var_rho[1,:]
	var_rho[M,:] = var_rho[M-1,:]
	#mask_rho = np.load('mask_rho.npy')
	#var_rho[:,mask_rho==0]=np.nan
	return (var_rho)	
	
def u2rho_2d(var_u):
	Mp,L = var_u.shape
	var_rho = np.zeros((Mp,L+1))
	var_rho[:,1:L-1] = 0.5*(var_u[:,0:L-2]+var_u[:,1:L-1]);
	var_rho[:,0] = var_rho[:,1];
	var_rho[:,L] = var_rho[:,L-1];
	#mask_rho = np.load('mask_rho.npy')
	#var_rho[:,mask_rho==0]=np.nan
	return(var_rho)




##---------------------------INTERPOLATION TOOLS---------------------------


def interp_wmop_to_hfr(ds, hfr, interp_kind='linear'):
    
    # Interp U
    u = ds.u[0,-1,:,:].values
    u[np.isnan(u)] = 100

    fu = interp.interp2d(ds.lon_u[1,:].values, ds.lat_u[:,1].values, u, kind=interp_kind)
    u_interp = fu(hfr['lon'], hfr['lat'])
    u_interp[u_interp>10] = np.nan
    
    u_interp = np.ma.masked_array(u_interp, mask=hfr['u_radar'].mask) # mask
    
    # Interp V
    v = ds.v[0,-1,:,:].values
    v[np.isnan(v)] = 100

    fv = interp.interp2d(ds.lon_v[1,:].values, ds.lat_v[:,1].values, v, kind=interp_kind)
    v_interp = fv(hfr['lon'], hfr['lat'])
    v_interp[v_interp>10] = np.nan
    
    v_interp = np.ma.masked_array(v_interp, mask=hfr['v_radar'].mask) # mask
    
    return u_interp, v_interp


from scipy import ndimage
from scipy.ndimage import distance_transform_edt

def find_virtual_argo(ds_wmop, lon_argo, lat_argo, min_distance=35):
    
    """
    Given the position of some Argo floats, the function returns the longitude and latitude indexes
    for the position of the virtual buoys, i.e, each position is calculated as the further point both 
    from coast or from other point.
    
    Inputs: 
        ds_wmop (to extract the coast mask and the latitude and longitude) 
        lon_argo, lat_argo
        min_distance: minimum distance permitted between buoys or land-buoys (fun(grid_cell))
    Outputs:
        Longitude and latitude index of the position of the buoy position 
    Author: Jaime Hernandez (jhernandez@socib.es)
    Date: 12-March-2019
    Last update: 1-February-2022
    
    """

    # Longitude and Latitude of the Argo profiles
    lon_wmop = ds_wmop.lon_rho.values[1,:]
    lat_wmop = ds_wmop.lat_rho.values[:,1]

    # Index of Nearest neighbour to Argo observations
    id_lon = [np.abs(lon_wmop-lon).argmin() for lon in lon_argo] 
    id_lat = [np.abs(lat_wmop-lat).argmin() for lat in lat_argo] 

    mask = ds_wmop.mask_rho.copy(deep=True).values 
    mask[id_lat,id_lon]=0        # mask positions of Argo buoys
    d = distance_transform_edt(mask)   # Compute distance to closest masked point

    id_lonv = []
    id_latv = []

    nd = 40  # number of cells to mask at each side of virtual observations

    while np.max(d) > min_distance:

        #idx = np.argwhere(d == np.max(d))
        id0 = np.argwhere(d == np.max(d))
        idy = id0[0][0]
        idx = id0[0][1]
        mask[idy-nd:idy+nd,idx-nd:idx+nd]=0
        d = distance_transform_edt(mask)

        id_lonv.append(idx)
        id_latv.append(idy)

    return id_lonv, id_latv





###################################################################################
####################   RANDOM tools     #######################

def set_cell_width(width):
    """" Set width as percentage of total width: values up to 100 """
    
    display(HTML("<style>.container { width:" + str(width) + "% !important; }</style>"))
    
    return
            
    
    
# Select experiments
def select_experiments(subset, experiment_list):
    """Give a list of positions within the experiment list dictionary
        Format -- { Name: [folder, color, label]... }
        """
    
    exp_list = [exp for i,exp in enumerate(experiment_list) if i in subset];  
    colors = [experiment_list[exp][1] for i,exp in enumerate(experiment_list) if i in subset]; 
    labels = [experiment_list[exp][2] for i,exp in enumerate(experiment_list) if i in subset]; 
    
    print(F' Selected experiments: {exp_list}')
    
    return exp_list, colors, labels

# Set Fonts
def set_font_size( fontsize=12, fontweight='bold', family='normal'):
    font = {'family' : family,
            'weight' : fontweight,
            'size'   : fontsize}

    matplotlib.rc('font', **font)




# Get unique times from mfdataset (gets more recent his file)
def get_unique_times_index(data):
    total_times = data.dims['ocean_time']
    times = data.ocean_time.values
    set_times = set()
    unique_index = []
    for t in reversed(range(total_times)):
        if times[t] not in set_times:
            set_times.add(times[t])
            unique_index.append(t)
    unique_index = list(reversed(unique_index))
    
    return unique_index




import seawater as sw
def plot_TS_diagram(salt, temp, dep, title=None, outfile=None, limits=None, ax1=None):
    
    """ Plot TS diagram for Temperature and salinity values, given as array"""
    # Depth bins
    dep1=100;
    dep2=400;
    dep3=800;

    
    ii = (~np.isnan(temp)) | (~np.isnan(salt))
    salt = salt[ii]
    temp = temp[ii]
    dep = dep[ii]

    # Figure out boudaries (mins and maxs)
    smin = salt.min() - (0.01 * salt.min())
    smax = salt.max() + (0.01 * salt.max())
    tmin = temp.min() - (0.1 * temp.max())
    tmax = temp.max() + (0.1 * temp.max())

    # Calculate how many gridcells we need in the x and y dimensions
    xdim = int(round((smax-smin)/0.1+1,0))
    ydim = int(round((tmax-tmin)+1,0))

    # Create empty grid of zeros
    dens = np.zeros((ydim,xdim))

    # Create temp and salt vectors of appropiate dimensions
    ti = np.linspace(1,ydim-1,ydim)+tmin
    si = np.linspace(1,xdim-1,xdim)*0.1+smin

    # Loop to fill in grid with densities
    for j in range(0,int(ydim)):
        for i in range(0, int(xdim)):
            dens[j,i]=sw.dens(si[i],ti[j],0)

    # Substract 1000 to convert to sigma-t
    dens = dens - 1000

    # Plot data ***********************************************
    if ax1 == None:
        fig1 = plt.figure()
        ax1 = fig1.add_subplot(111)
        
    CS = ax1.contour(si,ti,dens, linewidth=0.2,# levels=4,
                     linestyles='dashed', colors='k', alpha=0.6)
    plt.clabel(CS, fontsize=10, inline=1, fmt='%1.0f') # Label every second level


    ndep1 = (dep <= dep1)
    ndep2 = (dep > dep1) & (dep <= dep2)
    ndep3 = (dep > dep2) & (dep <= dep3)
    ndep4 = (dep > dep3)
    
    ax1.plot(salt[ndep1], temp[ndep1],'o', c='darkred',markersize=2, label='0-100m')
    ax1.plot(salt[ndep2], temp[ndep2],'o', c='steelblue',markersize=2, label='100-400m')
    ax1.plot(salt[ndep3], temp[ndep3],'o', c='grey',markersize=2, label='400-800m')
    ax1.plot(salt[ndep4], temp[ndep4],'o', c='black',markersize=2, label='>800m')

    ax1.set_xlabel('Salinity')
    ax1.set_ylabel('Temperature (C)')
    ax1.set_title(title, fontsize=14, fontweight='bold')
    ax1.legend()
    
    if limits:
        ax1.set_xlim((limits[0], limits[1]))
        ax1.set_ylim((limits[2], limits[3])) 
    
    if outfile:
        fig1.savefig(outfile, dpi=250, transparent=False, facecolor="w")

