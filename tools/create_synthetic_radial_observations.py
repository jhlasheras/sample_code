

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
import glob
from datetime import datetime
import scipy.interpolate as interp

import tools

def main():

    files = glob.glob('/mnt/vinson/DATA/jhernandez/WMOP_ASSIM/Observations/HFR_radial_nudging_Oct2014/assim_obs_HFR_radial_*.obs')

    for file in files:

        # Determine Date
        strdate = file[-12:-4]
        date = datetime.strptime(strdate,'%Y%m%d')

        # Read observations
        df = read_obsfile( file)

        # Read WMOP file for corresponding
        wmop_file = '/mnt/vinson/DATA/jhernandez/Model_Outputs/wmop_forecast_nr/roms_WMOP_FORECAST_{0}_avg.nc'.format(strdate)
        ds = xr.open_dataset(wmop_file)

        # LOAD HFR OBSERVATIONS
        import get_hfr_data
        hfr={}
        hfr['u_radar'], hfr['v_radar'], hfr['lon'], hfr['lat'] = get_hfr_data.get_Currents_HFR(strdate,1)

        # Interpolate WMOP fields to Observation points
        totals = {}
        totals['u_interp'], totals['v_interp'] = interp_wmop_to_hfr(ds, hfr)
        radials = {}
        radials['u_galf'], radials['v_galf'] = interp_wmop_to_radial(ds, df[df['var']=='rg'].reset_index(drop=True))
        radials['u_form'], radials['v_form'] = interp_wmop_to_radial(ds, df[df['var']=='rf'].reset_index(drop=True))

        # Project WMOP interpolated velocities to Radial Observations
        radials['galf'] = project_to_radial(radials['u_galf'], radials['v_galf'], df.bearing[df['var']=='rg'].reset_index(drop=True))
        radials['form'] = project_to_radial(radials['u_form'], radials['v_form'], df.bearing[df['var']=='rf'].reset_index(drop=True))

        # New file path
        file_new = '/DATA/jhernandez/WMOP_ASSIM/Observations/HFR_radial_nudging_Oct2014/assim_obs_HFR_virtual_radial_{0}.obs'.format(strdate)
        file_new = '/home/jhernandez/Escritorio/HFR_radial_nudging_Oct2014/assim_obs_HFR_virtual_radial_{0}.obs'.format(strdate)

        # Create new DataFrame with the virtual observations
        df_new = df.copy(deep=True)
        df_new.val[df['var']=='rg'] = np.array(radials['galf'])
        df_new.val[df['var']=='rf'] = np.array(radials['form'])

        # Drop NaN values
        df_new = df_new[np.isnan(df_new['val'])==False].reset_index(drop=True)

        # save observation DataFrame
        df_new.to_csv(file_new, header=None, sep=' ', index=False,  float_format='%.6f')

        print('Virtual Radial observation obsfile created for {0}'.format(date.strftime('%d-%b-%Y')))


        ## PLOT OBSERVATIONS

        fig = plt.figure(figsize=(15,14))

        # Radial Observations
        ax, gl = tools.make_map(continents=False, limits=[0.2,1.6,38.0,39.2], subplot_dim=221)
        plt.scatter(df.lon[df['var']=='rf'], df.lat[df['var']=='rf'], 20, df.val[df['var']=='rf'], transform=gl.crs)
        plt.colorbar()
        plt.title('Radials Observation Form \n {0}'.format(date.strftime('%d-%m-%Y')), fontsize=15, fontweight='bold')

        ax, gl = tools.make_map(continents=False, limits=[0.2,1.6,38.3,39.5], subplot_dim=222)
        plt.scatter(df.lon[df['var']=='rg'], df.lat[df['var']=='rg'], 20, df.val[df['var']=='rg'], transform=gl.crs)
        plt.colorbar()
        plt.title('Radials Observation Galf \n {0}'.format(date.strftime('%d-%m-%Y')), fontsize=15, fontweight='bold')

        # Virtual Radial
        ax, gl = tools.make_map(continents=False, limits=[0.2,1.6,38.3,39.5], subplot_dim=223)
        plt.scatter(df.lon[df['var']=='rf'], df.lat[df['var']=='rf'], 20, radials['form'], transform=gl.crs)
        plt.colorbar()
        plt.title('Virtual Radials Observation Form \n {0}'.format(date.strftime('%d-%m-%Y')), fontsize=15, fontweight='bold')

        ax, gl = tools.make_map(continents=False, limits=[0.2,1.6,38.3,39.5], subplot_dim=224)
        plt.scatter(df.lon[df['var']=='rg'], df.lat[df['var']=='rg'], 20, radials['galf'], transform=gl.crs)
        plt.colorbar()
        plt.title('Virtual Radials Observation Galf \n {0}'.format(date.strftime('%d-%m-%Y')), fontsize=15, fontweight='bold')

        fig.savefig('/home/jhernandez/Escritorio/HFR_radial_nudging_Oct2014/obs_HFR_radial_virtual_vs_real_{0}.png'.format(strdate), dpi=250)
        print('Comparison figure between real and virtual Radial observations created for {0}\n'.format(date.strftime('%d-%b-%Y')))




def read_obsfile(obs_input_file):
    
    # read observations
    df_obs = pd.read_csv(obs_input_file,
                      names=['var','source','year','month','day','hour','minute','lon','lat','depth','val','err','rep','bearing'],
                      delimiter=' ')
    return df_obs


def project_to_radial(u, v, bearing):
    """Project to radial observation given the u and v components of an observed velocity
    and the bearing with the antenna at the observation point"""
    
    module = np.sqrt(u**2 + v**2)
    angle = np.arctan2(v,u)
    theta = bearing*np.pi/180 - angle
    
    radial = module * np.cos(theta)
    
    return radial


def interp_wmop_to_hfr(ds, hfr):
    
    # Interp U
    u = ds.u[0,-1,:,:].values
    u[np.isnan(u)] = 100000

    fu = interp.interp2d(ds.lon_u[1,:].values, ds.lat_u[:,1].values, u, kind='linear')
    u_interp = fu(hfr['lon'], hfr['lat'])
    u_interp[u_interp>1000] = np.nan
    
    if 'u_radar' in hfr.keys():
        u_interp = np.ma.masked_array(u_interp, mask=hfr['u_radar'].mask) # mask
    
    # Interp V
    v = ds.v[0,-1,:,:].values
    v[np.isnan(v)] = 100000

    fv = interp.interp2d(ds.lon_v[1,:].values, ds.lat_v[:,1].values, v, kind='linear')
    v_interp = fv(hfr['lon'], hfr['lat'])
    v_interp[v_interp>1000] = np.nan
    
    if 'u_radar' in hfr.keys():
        v_interp = np.ma.masked_array(v_interp, mask=hfr['v_radar'].mask) # mask
    
    return u_interp, v_interp

def interp_wmop_to_radial(ds, df):
    
    # Interp U
    u = ds.u[0,-1,:,:].values
    u[np.isnan(u)] = 100000

    fu = interp.interp2d(ds.lon_u[1,:].values, ds.lat_u[:,1].values, u, kind='linear')
    u_interp = [fu(df['lon'][i], df['lat'][i]).tolist()[0] for i in range(len(df['lon']))]
    u_interp = np.array(u_interp)
    u_interp[u_interp>10] = np.nan
       
    # Interp V
    v = ds.v[0,-1,:,:].values
    v[np.isnan(v)] = 100000

    fv = interp.interp2d(ds.lon_v[1,:].values, ds.lat_v[:,1].values, v, kind='linear')
    v_interp = [fv(df['lon'][i], df['lat'][i]).tolist()[0] for i in range(len(df['lon']))]
    v_interp = np.array(v_interp)
    v_interp[v_interp>10] = np.nan
    
    return u_interp, v_interp



main()