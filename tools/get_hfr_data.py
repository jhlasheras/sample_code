#!/usr/bin/env python3
# -*- coding: utf-8 -*-



import numpy as np
import pandas as pd
import netCDF4
import datetime
import matplotlib.pyplot as plt
#from mpl_toolkits.basemap import Basemap




def get_Currents_HFR(strdate, dailymeanoption=1):


    assert type(strdate) is str, 'strdate is not a string {0}'.format(strdate)    
    date = datetime.datetime.strptime(strdate,'%Y%m%d')

    print ('    Downloading L1 HF RADAR Total velocities product for {0}-{1}-{2} from SOCIB thredds server from SOCIB thredds server ...'.format(date.day,date.month,date.year))

    filename = 'http://thredds.socib.es/thredds/dodsC/hf_radar/hf_radar_ibiza-scb_codarssproc001/L1/{0}/dep0001_hf-radar-ibiza_scb-codarssproc001_L1_{0}-{1:02}.nc'.format(date.year,date.month)

    try:
        ds = netCDF4.Dataset(filename)
        #print (ds.variables.keys())

        lon = ds.variables['LON'][:]
        lat = ds.variables['LAT'][:]
        nx = lon.shape[0]
        ny = lat.shape[0]

        time = ds.variables['time'][:]
        timeStr = ds.variables['time'].units.split()
        timeUnits = timeStr[0]
        refDate = datetime.datetime.strptime(timeStr[2],'%Y-%m-%d')

        #time = np.array([eval ('refDate + datetime.timedelta({0}=t)'.format(timeUnits)) for t in time])
        time = np.array([refDate + datetime.timedelta(seconds=t) for t in time])
        day = np.array([dt.day for dt in time])
        kd = np.where(day==date.day)[0]

        # Extract data for selected date
        time_data = time[kd]
        u_radar = ds.variables['U'][kd,:,:]
        v_radar = ds.variables['V'][kd,:,:]

        # QC flags
        qc_u = ds.variables['QC_U'][kd,:,:]
        qc_v = ds.variables['QC_V'][kd,:,:]


        # Mask data that are not QC=1,2,9 (good, probably good and missing data)
        u_radar = np.ma.masked_invalid(u_radar)
        v_radar = np.ma.masked_invalid(v_radar)
        u_radar.mask[(qc_u!=1) & (qc_u!=2) & (qc_u!=9)] = True
        v_radar.mask[(qc_v!=1) & (qc_v!=2) & (qc_v!=9)] = True

        #dailymeanoption =1

        if dailymeanoption ==1:

            threshold = 0.5   # Compute mean for pointwise availability of data over threshold
            num_nans = [np.sum(u_radar.mask,0)][0]
            u_mean = np.nanmean(u_radar,0)
            u_mean.mask[num_nans > threshold*u_radar.shape[0]] = True   # mask values for which availability of data is below threshold
            num_nans = [np.sum(v_radar.mask,0)][0]
            v_mean = np.nanmean(v_radar,0)
            v_mean.mask[num_nans > threshold*v_radar.shape[0]] = True

            u_radar = u_mean
            v_radar = v_mean

        return u_radar, v_radar, lon, lat


    except OSError:
        print (' ...File {0} not found fro the requested date'.format(filename))
        return False



def plotHFR(lon, lat, u_radar, v_radar, resolution='h', color='k'):

    lonmin = np.min(lon)
    lonmax = np.max(lon)
    latmin = np.min(lat)
    latmax = np.max(lat)

    m = Basemap(llcrnrlat=latmin-0.03, urcrnrlat=latmax+0.03,
            llcrnrlon=lonmin+0.03, urcrnrlon=lonmax+0.05, lat_ts=latmin, projection='merc',resolution='h')

    fig = plt.figure(figsize=(10,10))
    m.drawcoastlines(linewidth=.25)
    m.fillcontinents(color='0.7')
    m.drawparallels(np.arange(30.,40.,.3), labels=[1,0,1,0],linewidth=0.2)
    m.drawmeridians(np.arange(-10.,2.,.3), labels=[1,0,0,1],linewidth=0.2)
    
    # To have coordinates make a meshgrid and afterwards pass to projection coordinates

    lonf,latf = np.meshgrid(lon,lat)
    lx,ly = m(lonf,latf)

    # Plot arrows
    Q = m.quiver(lx,ly,u_radar,v_radar, color=color)
    qk = plt.quiverkey(Q, 0.1, 0.05, 0.2, r'$0.2 m/s$',
                    fontproperties={'weight': 'bold'})

    plt.show()

    return [m, fig, Q]



