import sys
sys.path.append('/home/jhernandez/Work//')
sys.path.append('/home/jhernandez/Work/python_tools/')

from defaults import *
import seawater as sw
from scipy import signal
from scipy.interpolate import griddata
from scipy import interpolate

def main():
    # Define date
    date = datetime(2009,9,4)

    df_sla = create_SLA_enatl_pseudoobs(date)
    df_sst = create_SST_enatl_pseudoobs(date)
    df_argo = create_ARGO_enatl_pseudoobs(date)

    df_allobs = pd.concat([df_sla, df_sst, df_argo]).reset_index(drop=True)
    output_filename = F"/home/jhernandez/Work/python_notebooks/EuroSea/data/obsfile_SLA-v2_SST_Argo_pseudo_enatl60_{date.strftime('%Y%m%d')}.obs"
    tools.write_obsfile(df_allobs, output_filename)


def create_SST_enatl_pseudoobs(date, resolution=5):

    # Define path
    file_sst_natl = F"/LOCALDATA/Data/eNATL60/SST/eNATL60MEDWEST-BLB002_y{date.strftime('%Ym%md%d')}.1h_sosstsst.nc"

    # Load SST Dataset
    ds_sst = xr.open_dataset(file_sst_natl)

    # Load mask
    mask_natl = xr.open_dataset('/LOCALDATA/Data/eNATL60/mask_eNATL60MEDWEST_3.6.nc')

    #lat = mask_natl.nav_lat[:,0]
    #lon = mask_natl.nav_lon[0,:]

    # Smoth field
    nb = int(np.floor(resolution))
    kernel = np.ones((nb,nb))/(nb*nb)

    sst = (ds_sst.sosstsst[7,:,:] / mask_natl.tmaskutil[0,:,:]).values
    sst = signal.convolve2d(sst, kernel, boundary='symm', mode='same')
    sst_error = (ds_sst.sosstsst.std(axis=0) / mask_natl.tmaskutil[0,:,:]).values
    sst_error = signal.convolve2d(sst_error, kernel, boundary='symm', mode='same')

    # Subsample
    lat_1d = mask_natl.nav_lat[::7,::7].values.flatten()
    lon_1d = mask_natl.nav_lon[::7,::7].values.flatten()

    sst_1d = sst[::7,::7].flatten()
    sst_error_1d = sst_error[::7,::7].flatten()

    # Create DataFrame with Observations
    df_sst = pd.DataFrame(data={'lon': lon_1d, 'lat': lat_1d, 'val': sst_1d, 'error': sst_error_1d})
    df_sst = df_sst[~df_sst.isin([np.nan, np.inf, -np.inf]).any(1)].reset_index(drop=True)

    rand_err_sst = [np.random.normal(0, sigma/4, 1)[0] for sigma in df_sst.error]

    # Errors
    repobserrSST=0.5*0.5    # variance 0.25 degC²   <=> std 0.5 degC
    insobserrSST=0.25*0.25  # variance 0.0625 degC² <=> std 0.25 degC
    err = repobserrSST + insobserrSST

    obs_err_infl_coeff=5    # to account for correlated errors in SST map
    mydepth_SST=0.0          # depth specified for SST data (0 - 10m)

    # Date for Observations
    date_obs = datetime.strptime(str(ds_sst.time_counter[7].values)[0:16], '%Y-%m-%dT%H:%M')
    date_obs.hour
    df_sst['year'] = date_obs.year
    df_sst['month'] = date_obs.month
    df_sst['day'] = date_obs.day
    df_sst['hour'] = date_obs.hour
    df_sst['minute'] = date_obs.minute
    # Errors
    df_sst['err'] = repobserrSST + insobserrSST
    df_sst['rep'] = obs_err_infl_coeff

    df_sst['var'] = 't'
    df_sst['source'] = 'SST_eNATL60'
    df_sst['depth'] = mydepth_SST  # Depth

    df_sst['val'] = df_sst['val'] + rand_err_sst

    variables = ['var', 'source', 'year', 'month', 'day', 'hour', 'minute', 'lon', 'lat', 'depth', 'val', 'err', 'rep']

    df_sst = df_sst[variables]
    print(F" SST pseudo observations generated from eNATL60 simulation for {date.strftime('%d-%m-%Y')}")

    return df_sst

def create_ARGO_enatl_pseudoobs(date):

    # Load Real Obsfile for Experiment date
    obsfile = '/home/jhernandez/Escritorio/obsfile_EuroSea_WP2_real_obs_SLA_SST_ARGO_20090904.obs'
    df_real = tools.read_obsfile(obsfile)

    # Subset Argo observations
    ii = df_real['source'].str.contains('ARGO')
    df_argo_real = df_real[ii]

    # Dataframe of Lon and Lat for Argo profiles
    argo_positions = df_argo_real[['lon','lat']].drop_duplicates().reset_index(drop=True)

    # Load mask
    mask_natl = xr.open_dataset('/LOCALDATA/Data/eNATL60/mask_eNATL60MEDWEST_3.6.nc')

    # eNATL60 lat and lon
    lat_natl = mask_natl.nav_lat[:,0].values
    lon_natl = mask_natl.nav_lon[0,:].values

    # Element of the lon and lat correspondant to Profile
    nlat = [np.argmin(np.abs(l - lat_natl)) for l in argo_positions['lat']]
    nlon = [np.argmin(np.abs(l - lon_natl)) for l in argo_positions['lon']]

    nprofs = argo_positions.shape[0]

    df_argo = pd.DataFrame()

    for i in range(nprofs):
        
        # Conditions to subset single profile
        cond1 = (df_argo_real['lon'] == argo_positions['lon'][i]) 
        cond2 = (df_argo_real['lat'] == argo_positions['lat'][i])
        cond3 = (df_argo_real['var'] == 't')
        cond4 = (df_argo_real['var'] == 's')

        # Subset Dataframe with single Temperature / Salinity profile 
        df_prof_t = df_argo_real.loc[cond1 & cond2 & cond3].reset_index(drop=True)
        df_prof_s = df_argo_real.loc[cond1 & cond2 & cond4].reset_index(drop=True)

        # Extract day of profile
        day_prof = datetime(df_prof_t['year'][0], df_prof_t['month'][0], df_prof_t['day'][0], df_prof_t['hour'][0], df_prof_t['minute'][0])
        
        # eNATL60 file 
        path_natl_3d = '/LOCALDATA/Data/eNATL60/WMED_3D-fields/'
        file_natl_temp = glob(F"{path_natl_3d}/*{day_prof.strftime('y%Ym%md%d')}*votemper.nc")[0]
        file_natl_salt = glob(F"{path_natl_3d}/*{day_prof.strftime('y%Ym%md%d')}*vosaline.nc")[0]

        # Load eNATL60 field
        ds_temp = xr.open_dataset( file_natl_temp ) 
        ds_salt = xr.open_dataset( file_natl_salt )
        
        # Interpolate (in time and depth) model to Argo observations
        interp_temp = ds_temp.votemper[:,:,nlat[i], nlon[i]].interp(deptht=df_prof_t['depth'], time_counter=day_prof)
        interp_salt = ds_salt.vosaline[:,:,nlat[i], nlon[i]].interp(deptht=df_prof_s['depth'], time_counter=day_prof)

        # Generate random noise to observations
        rand_err_temp = [np.random.normal(0, sigma, 1)[0] for sigma in df_prof_t['err'] ]
        rand_err_salt = [np.random.normal(0, sigma, 1)[0] for sigma in df_prof_s['err'] ]
        
        # Create new Dataframe as a copy of real obs
        df_prof_interp_t = df_prof_t.copy(deep=True)
        df_prof_interp_s = df_prof_s.copy(deep=True)
        
        # Substitute value of observation by model interpolated one
        df_prof_interp_t['val'] = interp_temp + rand_err_temp
        df_prof_interp_s['val'] = interp_salt + rand_err_salt
        
        df_argo = df_argo.append(df_prof_interp_t)
        df_argo = df_argo.append(df_prof_interp_s)

    df_argo['day'] = date.day
    df_argo['month'] = date.month

    df_argo['val'][df_argo['val']<10] = np.nan    
    df_argo = df_argo[~df_argo.isin([np.nan, np.inf, -np.inf]).any(1)].reset_index(drop=True)

    print(F" Argo pseudo observations generated from eNATL60 simulation for {date.strftime('%d-%m-%Y')}")

    return df_argo


def create_SLA_enatl_pseudoobs(date):

    files_altimetry = glob('/LOCALDATA/Data/EuroSea_WP2/eNATL60_pseudo-obs/pseudo*')

    time_window = 3
    ini_date = (date - timedelta(days=time_window)).strftime('%Y-%m-%d')
    end_date = (date + timedelta(days=time_window)).strftime('%Y-%m-%d')

    repobserrSSHaltimeter=0.03*0.03;      # variance 0.0009 m2 <=> std 0.03 m
    insobserrSSHaltimeter=0.02*0.02;      # variance 0.0004 m2 <=> std 0.02 m

    df_sla_all = pd.DataFrame()

    for f in files_altimetry[0:2]:
        # Load Dataset
        ds = xr.open_dataset(f)

        # Correct longitude and sumsample in time
        ds.longitude[ds.longitude>180] = ds.longitude[ds.longitude>180] - 360
        ds2 = ds.sel(time=slice(ini_date, end_date) )
        
        # Get SLA from DataSet
        df_sla0 = ds2[['sossheig', 'longitude', 'latitude']].to_dataframe()
        dates_sla = [ datetime.strptime(str(t), '%Y-%m-%d %H:%M:%S') for t in df_sla0.index ]

        df_sla = pd.DataFrame(data={'lon': df_sla0.longitude, 'lat': df_sla0.latitude, 'val': df_sla0.sossheig }).reset_index(drop=True)
        
        # Fill the dates for each observation
        df_sla['year'] = [d.year for d in dates_sla]
        df_sla['month'] = [d.month for d in dates_sla]
        df_sla['day'] = [d.day for d in dates_sla]
        df_sla['hour'] = [d.hour for d in dates_sla]
        df_sla['minute'] = [d.minute for d in dates_sla]
        # Errors
        df_sla['err'] = repobserrSSHaltimeter + insobserrSSHaltimeter
        df_sla['rep'] = 1

        df_sla['var'] = 'h'
        df_sla['source'] = f.split('/')[-1].split('_')[0]
        df_sla['depth'] = 0.0  # Depth
        
        # Generate random noise to observations
        rand_err_sla = [np.random.normal(0, sigma, 1)[0] for sigma in df_sla['err'] ]

        df_sla['val'] = df_sla['val'] + rand_err_sla

        variables = ['var', 'source', 'year', 'month', 'day', 'hour', 'minute', 'lon', 'lat', 'depth', 'val', 'err', 'rep']

        df_sla = df_sla[variables]

        df_sla_all = df_sla_all.append(df_sla)

    # Get MDT field
    file_mdt_natl = glob('/LOCALDATA/Data/eNATL60/MDT/*')[1]
    ds_mdt = xr.open_dataset(file_mdt_natl) 

    # Interpolator MDT
    mdt = ds_mdt.MDT.values
    mdt[np.isnan(mdt)] = 10
    f_mdt = interpolate.interp2d(ds_mdt.lon.values, ds_mdt.lat.values, mdt, kind='cubic')

    # Extract MDT values at real SLA observation points
    mdt_interp = [f_mdt(df_sla_all.iloc[i]['lon'], df_sla_all.iloc[i]['lat'])[0] for i in range(len(df_sla_all)) ]
    mdt_interp = np.array(mdt_interp)
    mdt_interp[np.abs(mdt_interp)>1] = np.nan

    # Extract MDT from SSH observations to generate SLA
    df_sla_all['val'] = df_sla_all['val'] - mdt_interp
    df_sla_all = df_sla_all[~df_sla_all.isin([np.nan, np.inf, -np.inf]).any(1)].reset_index(drop=True)

    print(F" Altimetry pseudo observations generated from eNATL60 simulation for {date.strftime('%d-%m-%Y')}")

    return df_sla_all


main()










