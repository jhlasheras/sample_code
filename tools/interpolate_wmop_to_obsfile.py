import sys
sys.path.append('/home/jhernandez/Work//')
sys.path.append('/home/jhernandez/Work/python_tools/')

from defaults import *
from tqdm import tqdm

import pyroms
from scipy.interpolate import interp1d



def merge_all_inn_files(files_inn, files_obs):
    """ Given a list of innovation and observation files from analyses
    create a single dataframe with all observations and innovations"""

    df = None

    for inn, obs in zip(files_inn, files_obs):
        df0 = tools.get_innovations(inn, obs)
        
        n = inn.split('/')
        if len(n[-2])==8:
            df0['assim_date'] = datetime.strptime(n[-2], '%Y%m%d')
        else:
            df0['assim_date'] = datetime(int(n[-4]), int(n[-3]), int(n[-2]))
        df0['residuals'] = df0['val'] - df0['analysis']
        
        if df is None:
            df = df0
        else:
            df = pd.concat([df, df0], ignore_index=True)

    # dates_obs = [datetime(df['year'][i], df['month'][i], df['day'][i]) for i in range(df.shape[0])]
    dates_obs = [datetime(df['year'][i], df['month'][i], df['day'][i], df['hour'][i], df['minute'][i]) for i in range(df.shape[0])]
    df['dates_obs'] = dates_obs

    df = df.rename(columns={"lon": "longitude", "lat": "latitude", "val": "value"})

    return df


def get_index_different_obs(df):

    k = {}

    k['sla'] = (df['nb_sou']==5)
    k['sst'] = (df['source'].str.contains('SST'))
    k['argo_t'] = ( (df['source'].str.contains('ARGO')) & (df['nb_sou']==1) )
    k['argo_s'] = ( (df['source'].str.contains('ARGO')) & (df['nb_sou']==2) )
    k['hfr_u'] = ( (df['source'].str.contains('HF_Radar')) & (df['nb_sou']==3) )
    k['hfr_v'] = ( (df['source'].str.contains('HF_Radar')) & (df['nb_sou']==4) )
    k['gli_t'] = ( (df['source'].str.contains('GLI')) & (df['nb_sou']==1) )
    k['gli_s'] = ( (df['source'].str.contains('GLI')) & (df['nb_sou']==2) )

    return k


def interpolate_wmop_to_sla_obs(df, simulations):

    k = get_index_different_obs(df)
    for sim in simulations:
        if sim not in df.keys():
            df[sim] = np.nan; 

    mdt_filename = '/mnt/vinson/data/modelling/WMOP_ASSIM/Inputs/roms_WMOP_HINDCAST_synthetic_201505_201804_mean.nc'
    ds_mdt = xr.open_dataset(mdt_filename)

    dates = np.unique(df['dates_obs'].values)  # by date

    for day_analysis in tqdm(dates):
        
        datestr = pd.to_datetime(str(day_analysis)).strftime('%Y%m%d')

        # Select Observations for Source and date
        df_day = df[(df['dates_obs'] == day_analysis) & k['sla'] ].reset_index(drop=True)
        index_obs = list(df[(df['dates_obs'] == day_analysis) & k['sla'] ].index)
    #     # Select Observations for Source and date
    #     df_day = df[(df['assim_date'] == day_analysis) & k['sla'] ].reset_index(drop=True)
    #     index_obs = list(df[(df['assim_date'] == day_analysis) & k['sla'] ].index)
        
        for sim in simulations:
        
            # Load WMOP
            file_date = glob(F"{simulations[sim]}*{datestr}*avg*")[0]
            #glob(F"/mnt/vinson/DATA/modelling/WMOP_REANALYSIS/Outputs/EUROSEA_T4_noGLIDERS/forecast_scratch/*{datestr}*avg*")[0]
            ds_date = xr.open_dataset(file_date)

            # Get SSH
            sla = ds_date.zeta[0,:,:].values - ds_mdt.zeta[0,:,:].values
            sla[np.isnan(sla)] = 100
            fz = interp.interp2d(ds_date.lon_rho[1,:].values, ds_date.lat_rho[:,1].values, sla, kind='linear')

            # Interp
            sla_interp = [fz(df_day.longitude[i], df_day.latitude[i])[0] for i in range(df_day.shape[0])]
            sla_interp = np.array(sla_interp)
            sla_interp[sla_interp > 0.5] = np.nan

            df[sim][index_obs] = sla_interp

    return df



def interpolate_wmop_to_sst_obs(df, simulations):

    k = get_index_different_obs(df)
    for sim in simulations:
        if sim not in df.keys():
            df[sim] = np.nan; 

   # dates = np.unique(df['assim_date'])   # by assim date
    dates = np.unique(dates_obs) 
    dates.sort

    # dates = np.unique(dates_obs)  # by date

    for day_analysis in tqdm(dates):
        
        datestr = pd.to_datetime(str(day_analysis)).strftime('%Y%m%d')

        # Select Observations for Source and date
        df_day = df[(df['assim_date'] == day_analysis) & k['sst'] ].reset_index(drop=True)
        index_obs = list(df[(df['assim_date'] == day_analysis) & k['sst'] ].index)
        
        for sim in simulations:
        
            # Load WMOP
            file_date = glob(F"{simulations[sim]}*{datestr}*avg*")[0]
            #glob(F"/mnt/vinson/DATA/modelling/WMOP_REANALYSIS/Outputs/EUROSEA_T4_noGLIDERS/forecast_scratch/*{datestr}*avg*")[0]
            ds_date = xr.open_dataset(file_date)

            # Get SSH
            sst = ds_date.temp[0,-1,:,:].values #- ds_mdt.zeta[0,:,:].values
            sst[np.isnan(sst)] = 100
            fsst = interp.interp2d(ds_date.lon_rho[1,:].values, ds_date.lat_rho[:,1].values, sst, kind='linear')

            # Interp
            sst_interp = [fsst(df_day.longitude[i], df_day.latitude[i])[0] for i in range(df_day.shape[0])]
            sst_interp = np.array(sst_interp)
            sst_interp[sst_interp > 30] = np.nan

            df[sim][index_obs] = sst_interp

    return df



def interpolate_wmop_to_insitu_obs(df, simulations):

    k = get_index_different_obs(df)
    for sim in simulations:
        if sim not in df.keys():
            df[sim] = np.nan; 


    file_date = glob(F"{simulations[next(iter(simulations))]}*his*")[0]

    grd = pyroms.grid.get_ROMS_grid('grid', hist_file=file_date, grid_file=file_date)
    zeta = grd.vgrid.z_r[:]

    dates = np.unique(df['assim_date'])   # by assim date
    # dates = np.unique(dates_obs) 
    dates.sort

    # dates = np.unique(dates_obs)  # by date
    # dates.sort

    for day_analysis in tqdm(dates):

        # datestr = pd.to_datetime(str(day_analysis)).strftime('%Y%m%d')
        datestr = pd.to_datetime(str(day_analysis + np.timedelta64(1,'D'))).strftime('%Y%m%d')

        cond1 = (df['assim_date'] == day_analysis)
        # cond1 = (df['dates_obs'] == day_analysis)
        cond2 = k['argo_t'] | k['gli_t']
        cond3 = k['argo_s'] | k['gli_s']
    #     # Select Observations for Source and date
    #     df_day_temp = df[(df['assim_date'] == day_analysis) & k['argo_t'] ].reset_index(drop=True)
    #     index_obs_temp = list(df[(df['assim_date'] == day_analysis) & k['argo_t'] ].index)

        # get Lon Lat of CTD casts
        lonlat_obs = df[cond1 & cond2 ][['longitude', 'latitude']].drop_duplicates()

        for sim in simulations:

            # Load WMOP
            # file_date = glob(F"{simulations[sim]}*{datestr}*avg*")[0]
            file_date = glob(F"{simulations[sim]}*{datestr}*his*")[0]
            #glob(F"/mnt/vinson/DATA/modelling/WMOP_REANALYSIS/Outputs/EUROSEA_T4_noGLIDERS/forecast_scratch/*{datestr}*avg*")[0]
            ds_date = xr.open_dataset(file_date)

            for i in range(lonlat_obs.shape[0]):

                # Get Lon Lat of specific profile
                lon0 = lonlat_obs.iloc[i]['longitude']
                lat0 = lonlat_obs.iloc[i]['latitude']

                nlon = np.argmin(np.abs(ds_date.lon_rho[0,:].values - lon0))
                nlat = np.argmin(np.abs(ds_date.lat_rho[:,0].values - lat0))

                cond4 = (df['longitude'] == lon0) & (df['latitude'] == lat0)

                # Select Observations for Source and date
                df_prof_temp = df[ cond1 & cond2 & cond4 ].reset_index(drop=True)
                index_obs_temp = list(df[ cond1 & cond2 & cond4 ].index)


                z_obs = df_prof_temp['depth'].values.copy()
                z0 = zeta[:,nlat,nlon]
                z_roms = -z0

                # Interpolate
                f = interp1d(z_roms, ds_date.temp[0,:,nlat,nlon].values, fill_value="extrapolate")
                t_interp = f(z_obs)

                df[sim][index_obs_temp] = t_interp

                # Select Observations for Source and date
                df_prof_salt = df[ cond1 & cond3 & cond4 ].reset_index(drop=True)
                index_obs_salt = list(df[ cond1 & cond3 & cond4 ].index)

    #             z_obs = df_prof_salt['depth'].values[::-1].copy()
                z_obs = df_prof_salt['depth'].values.copy()
                z0 = zeta[:,nlat,nlon]
                z_roms = -z0

                # Interpolate
                f = interp1d(z_roms, ds_date.salt[0,:,nlat,nlon].values, fill_value="extrapolate")
                s_interp = f(z_obs)

                df[sim][index_obs_salt] = s_interp


    return df




def interpolate_wmop_to_hfr_obs(df, simulations):

    k = get_index_different_obs(df)
    for sim in simulations:
        if sim not in df.keys():
            df[sim] = np.nan; 

    dates = np.unique(df['assim_date'])   # by assim date
    dates.sort

    # dates = np.unique(dates_obs)  # by date

    for day_analysis in tqdm(dates):
        
        # datestr = pd.to_datetime(str(day_analysis)).strftime('%Y%m%d')
        datestr = pd.to_datetime(str(day_analysis + np.timedelta64(1,'D'))).strftime('%Y%m%d')

        # Select Observations for Source and date
        df_day_u = df[(df['assim_date'] == day_analysis) & k['hfr_u'] ].reset_index(drop=True)
        df_day_v = df[(df['assim_date'] == day_analysis) & k['hfr_v'] ].reset_index(drop=True)
        index_obs_u = list(df[(df['assim_date'] == day_analysis) & k['hfr_u'] ].index)
        index_obs_v = list(df[(df['assim_date'] == day_analysis) & k['hfr_v'] ].index)
        
        for sim in simulations:
        
            # Load WMOP
            file_date = glob(F"{simulations[sim]}*{datestr}*avg*")[0]
            #glob(F"/mnt/vinson/DATA/modelling/WMOP_REANALYSIS/Outputs/EUROSEA_T4_noGLIDERS/forecast_scratch/*{datestr}*avg*")[0]
            ds_date = xr.open_dataset(file_date)

            # Get SSH
            U = ds_date.u[0,-1,:,:].values 
            V = ds_date.v[0,-1,:,:].values
            U[np.isnan(U)] = 100
            V[np.isnan(V)] = 100
            fu = interp.interp2d(ds_date.lon_u[1,:].values, ds_date.lat_u[:,1].values, U, kind='linear')
            fv = interp.interp2d(ds_date.lon_v[1,:].values, ds_date.lat_v[:,1].values, V, kind='linear')

            # Interp
            u_interp = [fu(df_day_u.longitude[i], df_day_u.latitude[i])[0] for i in range(df_day_u.shape[0])]
            v_interp = [fv(df_day_v.longitude[i], df_day_v.latitude[i])[0] for i in range(df_day_v.shape[0])]
            u_interp = np.array(u_interp)
            v_interp = np.array(v_interp)
            u_interp[u_interp > 1.5] = np.nan
            v_interp[v_interp > 1.5] = np.nan

            df[sim][index_obs_u] = u_interp
            df[sim][index_obs_v] = v_interp

    return df


def create_TS_dataframe(df, simulations):
    """ Given a dataframe of observations creates other with values of 
    Temperature and Salinity in separated variables, ready for plotting TS diagrams"""
    
    k = get_index_different_obs(df)

    columns_salt = {'value': 'Salinity Obs'}
    columns_temp = {'value': 'Temperature Obs'}

    for sim in simulations:
        columns_salt[sim] = F"Salinity {sim}"
        columns_temp[sim] = F"Temperature {sim}"
        
    fields_subset = ['longitude', 'latitude', 'depth', 'value', 'dates_obs'] + list(simulations.keys())

    # Subset Important variables for TS analysis
    df_gli_temp = df[k['gli_t']][fields_subset].reset_index(drop=True)
    df_gli_salt = df[k['gli_s']][fields_subset].reset_index(drop=True)

    # Rename columns with salinity and temp labels
    df_gli_temp = df_gli_temp.rename(columns=columns_temp)
    df_gli_salt = df_gli_salt.rename(columns=columns_salt)

    # Merge Dataframes
    df_full = pd.merge(df_gli_temp, df_gli_salt, how="left", on=["longitude", "latitude", "depth", "dates_obs"])
    df_full['dates_obs'] = pd.to_datetime(df_full['dates_obs'])

    df_full = df_full.set_index('dates_obs')

    return df_full
