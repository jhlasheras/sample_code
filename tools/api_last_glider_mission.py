import requests
import json
import pandas
import os
from datetime import datetime, timedelta

from api_utils import *

def main():

    #SOCIB API settings
    api_key = 'api_dummy' #write you SOCIB api_key => get it from http://api.socib.es/home/ (formulary at the bottom of the page)
    api_url = 'http://api.socib.es'
    headers = {
        'accept': 'application/vnd.socib+json',
        'apikey': api_key,
    }
    apiCache = {}

    end_point = '/entries/'

    initial_datetime =  (datetime.now() - timedelta(days=365)).strftime('%Y-%m-%dT%H:%M')
    end_datetime =  datetime.now().strftime('%Y-%m-%dT%H:%M')
    processing_level = 'L2'
    platforms_id = 'Glider'
    data_mode = None

    query_parameters = 'processing_level=' + processing_level + '&initial_datetime=' + initial_datetime + '&end_datetime=' + end_datetime + '&bbox=35.6,45.6,-6.0,9.8'

    if data_mode:
      query_parameters = query_parameters + '&data_mode=' + data_mode

    ## GET Entries for QUERY
    entries = []
    for platform_id in platforms_id:
      url = '%s%s?%s&platform=%s' % (api_url, end_point, query_parameters, platform_id)
      while url != None:
          #print(url)
          request_ = requests.get(url, headers=headers)
          response_ = json.loads(request_.text)
          for entry in response_['results']:#loop over netCDFs (entries)
              data = {}
              data['platform_id'] = platform_id
              #inherited info
              data['id'] = entry['id']
              data['processing_level'] = entry['processing_level']
              data['data_mode'] = entry['data_mode']
              data['initial_datetime'] = entry['initial_datetime']
              data['end_datetime'] = entry['end_datetime']
              data['opendap_url'] = entry['services']['opendap']['url']
              data['http_url'] = entry['services']['http_file']['url']
              data['catalog_url'] = entry['services']['thredds_catalog']['url']
              data['variables'] = entry['parameters']
              data['instrument'] = entry['instrument']
              #derived info
              data['netCDF_name'] = entry['services']['opendap']['url'].split('/')[-1]
              #implicit info
              data, apiCache = variables_details(data,apiCache) #variables handler
              data, apiCache = instrument_details(data,apiCache) #instrument handler
              entries.append(data)
          url = response_['next']

    # Create DataFrame with Entries
    ordered_header = ['instrument_category', 'instrument_type', 'instrument_name', 'processing_level', 'data_mode', 'initial_datetime', 'end_datetime', 'variables', 'netCDF_name', 'id', 'opendap_url', 'http_url', 'catalog_url']
    nc_dataframe = pandas.DataFrame.from_dict(entries)#.sort_values(ordered_header)
    nc_dataframe = nc_dataframe[ordered_header]

    nc_dataframe = nc_dataframe.sort_values(by='end_datetime', ascending=False).reset_index(drop=True)

    for i in nc_dataframe.iloc[0:4]['opendap_url']:
        print(i)

main()
