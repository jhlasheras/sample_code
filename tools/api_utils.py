import requests
import json
import pandas
import os
from datetime import datetime, timedelta


#SOCIB API settings
api_key = 'api_dummy' #write you SOCIB api_key => get it from http://api.socib.es/home/ (formulary at the bottom of the page)
api_url = 'http://api.socib.es'
headers = {
    'accept': 'application/vnd.socib+json',
    'apikey': api_key,
}
apiCache = {}


#AUXILIARY FUNCTIONS
def instrument_details(data, apiCache, entry):
    #it handles the implicit info retourned as value for the intrument; in particular, it will return its name, type and category
    url = entry['instrument']
    if url != None:
        #instrument name
        try:
            response = apiCache[url]
        except:
            request = requests.get(url, headers=headers)
            apiCache[url] = json.loads(request.text)
            response = apiCache[url]
        data['instrument_name'] = response['name']
        #instrument type and category
        url = response['type']
        try:
            response = apiCache[url]
        except:
            request = requests.get(url, headers=headers)
            apiCache[url] = json.loads(request.text)
            response = apiCache[url]

        data['instrument_type'] = response['name']
        data['instrument_category'] = response['type']

    else: #aggregations case
        data['instrument_name'] = 'N/A'
        data['instrument_type'] = 'N/A'
        data['instrument_category'] = 'N/A'
    return [data,apiCache]


def variables_details(data, apiCache, entry):
    #it handles the implicit info retourned as value for the variables; in particular it will return the variable code.
    data['variables'] = ''
    url = entry['variables']
    try:
        response = apiCache[url]
    except:
        request = requests.get(url, headers=headers)
        apiCache[url] = json.loads(request.text)
        response = apiCache[url]
    for variable in response:
        url = variable['variable']
        try:
            response = apiCache[url]
        except:
            request = requests.get(url, headers=headers)
            apiCache[url] = json.loads(request.text)
            response = apiCache[url]

        try:
            if (variable['processing_level'] == data['processing_level']):
                data['variables'] = data['variables'] +','+ response['code']
        except:
            pass
    #data['variables'] = list( set( data['variables'].split(',')[1:] ) )
    return [data, apiCache]


#AUXILIARY FUNCTIONS
def instrument_details(data, apiCache):
    #it handles the implicit info retourned as value for the intrument; in particular, it will return its name, type and category
    url = data['instrument']
    if url != None:
        #instrument name
        try:
            response = apiCache[url]
        except:
            request = requests.get(url, headers=headers)
            apiCache[url] = json.loads(request.text)
            response = apiCache[url]
        data['instrument_name'] = response['name']
        #instrument type and category
        url = response['type']
        try:
            response = apiCache[url]
        except:
            request = requests.get(url, headers=headers)
            apiCache[url] = json.loads(request.text)
            response = apiCache[url]
        data['instrument_type'] = response['name']
        data['instrument_category'] = response['type']
    else: #aggregations case
        data['instrument_name'] = 'N/A'
        data['instrument_type'] = 'N/A'
        data['instrument_category'] = 'N/A'
    return [data,apiCache]

def variables_details(data, apiCache):
    #it handles the implicit info returned as value for the parameters; in particular it will return its name and its corresponding std variable code.
    url = data['variables']
    data['variables'] = ''
    try:
        response = apiCache[url]
    except:
        request = requests.get(url, headers=headers)
        apiCache[url] = json.loads(request.text)
        response = apiCache[url]
    for parameter in [param for param in response if param['standard_variable'] != None]:
        url = parameter['standard_variable']
        try:
            response = apiCache[url]
        except:
            request = requests.get(url, headers=headers)
            apiCache[url] = json.loads(request.text)
            response = apiCache[url]
        try:
            data['variables'] = data['variables'] +','+ response['code'] +' ('+parameter['name']+')'
        except:
            pass
    return [data, apiCache]


def get_platforms_type():
    url = 'http://api.socib.es/platform-types/?api_key=api_dummy'
    request = requests.get(url, headers=headers)
    response = json.loads(request.text)
    platforms = []
    for platform in response:
        platforms.append( platform['code'])

    return platforms

def get_platforms_id( platform_type):

    end_point = '/platforms/'
    query_parameters = 'platform_type='+platform_type
    platforms_id = []
    url = '%s%s?%s' % (api_url, end_point, query_parameters)

    while url != None:
            #print(url)
            request = requests.get(url, headers=headers)
            response = json.loads(request.text)
            for platform in response['results']:#loop over netCDFs (entries)
                    platforms_id.append(platform['id'])
            url = response['next']

    return platforms_id



def API_query():

    #SOCIB API SETTINGS
    api_key = 'api_dummy' #write your SOCIB api_key => get it from http://api.socib.es/home/ (formulary at the bottom of the page)
    api_url = 'http://api.socib.es'
    headers = {
        'accept': 'application/vnd.socib+json',
        'apikey': api_key,
    }
    apiCache = {}
    apiCache = {}

    # Select Platform Type

    print('Platform types:\n')
    platforms_type = get_platforms_type()
    for p in platforms_type:
        print (F'\t - {p}')
    print('')
    platform_type = input('Select a platform type:  ')

    while not platform_type in platforms_type:
        platform_type = input('Not Valid entry. Try again:  ')

    # Select Platform ID
    platforms_id = get_platforms_id( platform_type)

    print(F'\n{platform_type} has the following platforms ids:\n')
    for p in platforms_id:
        print(F'\t {p}')

    platforms_selected = input("\nSelect list of platforms (e.g: platform1, platform2 ) or press enter: ")

    if platforms_selected != '':
        platforms_selected = platforms_selected.split(', ')
        for plat in platforms_selected:
            if plat not in platforms_id:
                print('Incorrect entry: ' + plat)
                return 'Incorrect entry: ' + plat
        platforms_id = platforms_selected

    end_point = '/entries/'
    # Bounding Box
    bbox = '35.6,45.6,-6.0,9.8' #min. lat., max. lat., min. lon., max. lon

    # Set QUERY Parameters
    processing_level = input("\nSelect processing level: (L0, L1, L1_corr, L2) or press enter to continue ")
    initial_datetime = input("\nSelect initial date:  e.g. 2016-01-01T00:00:00  or press enter to continue ")
    end_datetime = input("\nSelect end date:  e.g. 2018-01-01T00:00:00  or press enter to continue ")
    data_mode = input("\nSelect data mode (rt, dt, dm) : ")

    if initial_datetime == '':
        initial_datetime =  '2009-01-01T00:00'

    if end_datetime == '':
        end_datetime =  datetime.now().strftime('%Y-%m-%dT%H:%M')

    query_parameters = 'processing_level=' + processing_level + '&initial_datetime=' + initial_datetime + '&end_datetime=' + end_datetime + '&bbox=' + bbox

    if data_mode:
        query_parameters = query_parameters + '&data_mode=' + data_mode

    ## GET Entries for QUERY
    entries = []
    for platform_id in platforms_id:
        url = '%s%s?%s&platform=%s' % (api_url, end_point, query_parameters, platform_id)
        while url != None:
            #print(url)
            request_ = requests.get(url, headers=headers)
            response_ = json.loads(request_.text)
            for entry in response_['results']:#loop over netCDFs (entries)
                data = {}
                data['platform_id'] = platform_id
                #inherited info
                data['id'] = entry['id']
                data['processing_level'] = entry['processing_level']
                data['data_mode'] = entry['data_mode']
                data['initial_datetime'] = entry['initial_datetime']
                data['end_datetime'] = entry['end_datetime']
                data['opendap_url'] = entry['services']['opendap']['url']
                data['http_url'] = entry['services']['http_file']['url']
                data['catalog_url'] = entry['services']['thredds_catalog']['url']
                data['variables'] = entry['parameters']
                data['instrument'] = entry['instrument']
                #derived info
                data['netCDF_name'] = entry['services']['opendap']['url'].split('/')[-1]
                #implicit info
                data, apiCache = variables_details(data,apiCache) #variables handler
                data, apiCache = instrument_details(data,apiCache) #instrument handler
                entries.append(data)
            url = response_['next']

    # Create DataFrame with Entries
    ordered_header = ['instrument_category', 'instrument_type', 'instrument_name', 'processing_level', 'data_mode', 'initial_datetime', 'end_datetime', 'variables', 'netCDF_name', 'id', 'opendap_url', 'http_url', 'catalog_url']
    nc_dataframe = pandas.DataFrame.from_dict(entries)#.sort_values(ordered_header)
    nc_dataframe = nc_dataframe[ordered_header]

    return nc_dataframe
