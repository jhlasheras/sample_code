import sys
import pandas as pd
from datetime import datetime

def main():
    """ Read obsfile from HFR experiments for a given date and creates another obfile just containing the SSH and ST observations """

    t0 = datetime(int(sys.argv[1]), int(sys.argv[2]), int(sys.argv[3]))  # Create datetime for the date given arguments

    obsfile_old = '/data/jhernandez/WMOP_ASSIM_OPERATIONAL/Observations/HFR_all_nudging_Oct2014/assim_obs_SLA_SSH_Argo_HFR_{0}.obs'.format(t0.strftime('%Y%m%d'))
    if len(sys.argv) > 4:
        obsfile_new = sys.argv[4]
    else:
        obsfile_new = '/DATA/jhernandez/WMOP_ASSIM/Observations/HFR_radials_nudging_Oct2014/assim_obs_without_ARGO_{0}.obs'.format(t0.strftime('%Y%m%d'))
    #obsfile_new = '/home/jhernandez/Escritorio/assim_obs_SLA_SSH_{0}.obs'.format(t0.strftime('%Y%m%d'))
    
    # read observations
    df = pd.read_csv(obsfile_old,
                        names=['var','source','year','month','day','hour','minute','lon','lat','depth','val','err','rep'],
                        delimiter=' ')

    # Keep just SST and SSH observations
    df = df[(df['source']!='ARGO') | (df['source']!='ARGOv')]

    df.to_csv(obsfile_new, header=None, sep=' ', index=False, float_format='%.6f')

    print('Obsfile created only containing the SST and SSH observations \n Input file: {1} \n Output_file {0} '.format(obsfile_new, obsfile_old))

main()