import sys
sys.path.append('/home/jhernandez/Work//')
sys.path.append('/home/jhernandez/Work/python_tools/')

from defaults import *
from tqdm import tqdm

import seawater as sw
from collections import defaultdict

from scipy import interpolate


experiments = ['FreeRun', 'GLIDERS', 'noGLIDERS']

for experiment in experiments:

    files_ssh_aug = glob(F'/mnt/vinson/DATA/modelling/WMOP_REANALYSIS/Outputs/EUROSEA_T4_{experiment}/forecast_scratch/roms_WMOP_FORECAST_201708*avg.nc')
    files_ssh_sep = glob(F'/mnt/vinson/DATA/modelling/WMOP_REANALYSIS/Outputs/EUROSEA_T4_{experiment}/forecast_scratch/roms_WMOP_FORECAST_201709*avg.nc')
    files_ssh_oct = glob(F'/mnt/vinson/DATA/modelling/WMOP_REANALYSIS/Outputs/EUROSEA_T4_{experiment}/forecast_scratch/roms_WMOP_FORECAST_201710*avg.nc')
    files_ssh_nov = glob(F'/mnt/vinson/DATA/modelling/WMOP_REANALYSIS/Outputs/EUROSEA_T4_{experiment}/forecast_scratch/roms_WMOP_FORECAST_201711*avg.nc')
    files_ssh_dec = glob(F'/mnt/vinson/DATA/modelling/WMOP_REANALYSIS/Outputs/EUROSEA_T4_{experiment}/forecast_scratch/roms_WMOP_FORECAST_201712*avg.nc')

    files_ssh = files_ssh_aug + files_ssh_sep + files_ssh_oct + files_ssh_nov + files_ssh_dec
    files_ssh.sort()

    ds_ssh = xr.open_mfdataset(files_ssh)

    path_mercator = '/LOCALDATA/Data/EuroSea_WP4_GLD/Mercator/Daily_fields/'

    ds_mask = xr.open_dataset( glob(F"{path_mercator}mask*")[0] )
    ds_m = xr.open_dataset( glob(F"{path_mercator}IBI*")[0] )

    x, y = np.meshgrid(ds_mask.lon.values, ds_mask.lat.values)

    lon_interp = ds_mask.lon.values
    lat_interp = ds_mask.lat.values

    ds_zeta = ds_ssh.zeta.where( (ds_ssh.lon_rho > -1.2) & (ds_ssh.lon_rho < 6.2), drop=True)
    ds_zeta = ds_zeta.where( (ds_zeta.lat_rho > 37.8) & (ds_zeta.lat_rho < 44.2), drop=True)

    ds_sst = ds_ssh.temp[:,-1,:,:].where( (ds_ssh.lon_rho > -1.2) & (ds_ssh.lon_rho < 6.2), drop=True)
    ds_sst = ds_sst.where( (ds_sst.lat_rho > 37.8) & (ds_sst.lat_rho < 44.2), drop=True)

    ny, nx = x.shape
    nt = ds_zeta.ocean_time.shape[0]

    zeta = np.empty((nt, ny, nx)) * np.nan

    for i in tqdm(range(nt)):
        
        zeta[i,:,:] = interpolate.griddata( (ds_zeta.lon_rho.values.flatten(), 
                                            ds_zeta.lat_rho.values.flatten()), 
                                            ds_zeta[i,:,:].values.flatten(), 
                                            (x, y))
            
    sst = np.empty((nt, ny, nx)) * np.nan

    for i in tqdm(range(nt)):
        
        sst[i,:,:] = interpolate.griddata( (ds_sst.lon_rho.values.flatten(), 
                                            ds_sst.lat_rho.values.flatten()), 
                                            ds_sst[i,:,:].values.flatten(), 
                                            (x, y))



    ds_wmop = xr.Dataset( data_vars=dict(
        SST=(["time","lat","lon"], sst),
        SSH=(["time","lat","lon"], zeta),),
                        coords=dict(
                            lon=(["lon"], ds_mask.lon.values),
                            lat=(["lat"], ds_mask.lat.values),
                            time=ds_sst.ocean_time.values,),
                        attrs=dict(description=F"WMOP reanalysis surface fields {experiment}"),
                        )


    ds_wmop.to_netcdf(F'/LOCALDATA/Data/EuroSea_WP4_GLD/wmop_surface_fields_{experiment}.nc')