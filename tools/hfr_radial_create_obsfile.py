import pandas as pd
from datetime import datetime
from datetime import timedelta


# Main code:

stations = {'form':['FORM','rf'],'galf':['GALF','rg']}
#t0 = datetime(2014,9,20)

import sys

def main():
    """ Create an obsfile in the shape needed for the WMOP DA system with the HFR radial daily mean observation
    for a given day.
    Inputs: Year Month Day given as arguments
        Output_file may be given as a forthS argument
        threshold may de given as a fifth argument
    (Eg: python hfr_radial_create_obsfile yyyy mm dd)  """

    t0 = datetime(int(sys.argv[1]), int(sys.argv[2]), int(sys.argv[3]))  # Create datetime for the date given arguments

    threshold = 12
    if len(sys.argv) > 5:
        threshold = float(sys.argv[5])
        
    if len(sys.argv) > 4:
        output_file = sys.argv[4]
    else:
        output_file = '/home/jhernandez/Escritorio/radial_observations/radials_mod_bearing_thres50/assim_obs_HFR_radial_{0}.obs'.format(t0.strftime('%Y%m%d'))

    df={}  # Empty dictionary

    for name in stations:

        df[name] = get_daily_observations(t0, stations[name][0])
    
        if df[name].empty == True:
            variables = ['var', 'source', 'year','month','day','hour','minute','lon','lat','depth','vel','err','rep', 'bearing']
            df[name] = pd.DataFrame(columns=variables)
            continue

        df[name] = compute_daily_mean(df[name], t0, threshold=threshold)


        # Add fields
        df[name]['source'] = 'HF_Radar'
        df[name]['var'] = stations[name][1]
        df[name]['depth'] = 0.0
        df[name]['err'] = 0.01
        df[name]['rep'] = 1
        # Add time fields
        df[name]['year'] = t0.year
        df[name]['month'] = t0.month
        df[name]['day'] = t0.day
        df[name]['hour'] = t0.hour
        df[name]['minute'] = t0.minute

        variables = ['var', 'source', 'year','month','day','hour','minute','lon','lat','depth','vel','err','rep', 'bearing']
        df[name] = df[name][variables]

    df_total = pd.concat([df['form'], df['galf']]).reset_index(drop=True)
    df_total['vel'] = -df_total['vel']/100
    # save observation DataFrame
    df_total.to_csv(output_file, header=None, sep=' ', index=False, float_format='%.6f')
    print('HFR radial obsfile for {0} created at {1}'.format(t0.strftime('%d-%m-%Y'), output_file))




def get_daily_observations(t0, station):

    """ Computes daily mean field of the HFR radial observations for a given day and for the station selected
        *Check the path
        t0 is a date given in datetime format (Eg: t0 = datetime(2014,9,20) )
        station = FORM / GALF"""

    t = t0

    # Create empty DataFrame to store all radial observations
    df_total = pd.DataFrame(columns=['lon','lat', 'vel','range','bearing','time'])

    for i in range(24):

        #file = '/LOCALDATA/Radiales/raw/Radial_rawInput/{0}/RDLm_{0}_{1}.ruv'.format(station,t.strftime('%Y_%m_%d_%H%M'))
        file = '/DATA/jhernandez/Observations/HFR/Radial_rawInput/{0}/RDLm_{0}_{1}.ruv'.format(station,t.strftime('%Y_%m_%d_%H%M'))

        f = open(file,'rb')

        # Read header information and neglect it
        nlines=[]
        while nlines==[]:
            l = f.readline()
            if 'TableRows' in str(l):
                nlines = int(l.split()[1])
                
        if nlines < 50:
            t = t+timedelta(hours=1)
            continue
            
        f.readline()
        names = f.readline().split()[1:]
        units = f.readline().split()[1:]
        ##print(nlines)

        # Initialize variables
        lon = []
        lat = []
        vel = []
        dist = []
        bear = []
        qc_x = []
        qc_t = []

        # Read Radial observations
        for ii in range(nlines):
            line = f.readline().strip()
            columns = line.split()

            lon.append(float(columns[0]))
            lat.append(float(columns[1]))
            vel.append(float(columns[15]))
            dist.append(float(columns[13]))
            bear.append(float(columns[14]))
            qc_x.append(float(columns[5]))
            qc_t.append(float(columns[6]))

        # Create Dataframe
        data={'lon':lon, 'lat':lat, 'vel':vel, 'range':dist, 'bearing':bear, 'qc_x':qc_x, 'qc_t':qc_t}
        df_radials = pd.DataFrame(data, columns=['lon','lat', 'vel','range','bearing','qc_x','qc_t'])
        # Add time
        df_radials['time']=t

        # Merge all daily observations in a DataFrame
        df_total = pd.concat([df_total,df_radials], sort=False)

        t = t+timedelta(hours=1)

    if df_total.shape[0]==0:
        print('NO observations for station {0}'.format(station))
        
        return df_total
    
    df_total = df_total.reset_index(drop=True)
    # Drop values which do not satisfy quality conditions
    df_total = df_total[(df_total['qc_x']<7) & (df_total['qc_t']<7) & (df_total['range']>2)].reset_index(drop=True)

    return df_total


def compute_daily_mean(df_total, t0, threshold=12):

    lonlat = df_total.drop_duplicates(['lon','lat','range','bearing'])[['lon','lat']]
    lonlat = lonlat.reset_index(drop=True)

    # Create empty DataFrame
    df_mean = pd.DataFrame(columns=['lon','lat', 'vel','range','bearing'])

    for i in range(len(lonlat)):

        # Create DataFrame with observations for every pair of Longitude and Latitude
        df_lonlat = df_total[(df_total['lon']==lonlat['lon'][i]) & (df_total['lat']==lonlat['lat'][i])]

        # Compute mean for every single point observations and concatenate with rest of the domain
        if len(df_lonlat)>threshold:
            df0 = pd.DataFrame(data=df_lonlat.mean()).transpose()
            df_mean = pd.concat([df_mean, df0], sort=False)


    df_mean = df_mean.reset_index().drop(['index'],axis=1)
    df_mean['time'] = t0

    return df_mean

main()
