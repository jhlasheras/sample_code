#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 15 16:46:42 2018

@author: jhernandez
"""
import glob
import datetime
import xarray as xr
import numpy as np
import netCDF4
import pandas as pd

drifters_path = '/home/jhernandez/Escritorio/Drifters/10_2014/files/'

files = glob.glob(drifters_path + '*nc')


#Initialize dateref
dateref = datetime.datetime(2014,10,1)
for i in range(15):
    
    # Initialize lists and arrays
    file_names = []
    initial_time = []
    end_time = []
    time_start = []
    lon_start = []
    lat_start = []    
    
    
    for f in files:
        #f = files[0]
       #print (f.rsplit('/')[-1])
        file_names.append(f.rsplit('/')[-1])
        
        # Load time
        ds = netCDF4.Dataset(f)
        time = ds.variables['time'][:]
        time = [datetime.datetime(1970,1,1) + datetime.timedelta(seconds=t) for t in time]
        
        #print('min time = {0}'.format(min(time)))
        #print('max time = {0}'.format(max(time)))
        initial_time.append(min(time))
        end_time.append(max(time))
        
        # Load longitude and latitude
        lon = ds.variables['LON'][:]
        lat = ds.variables['LAT'][:]
        
        # Find Closest time to the beginning of the simulation 1st October 2014        
        ini_time = min(time, key=lambda d: abs(d - dateref))
        pos_ini_time = time.index(ini_time)
        
        time_start.append(ini_time)
        lon_start.append( str(lon[pos_ini_time])[:6])
        lat_start.append( str(lat[pos_ini_time])[:6])
        #print ('Closest time to 1-10-2014 = {0}'.format(ini_time))
        #print('\n')
        
    # Create Dataframe and save it as csv
    output_filename = '/home/jhernandez/Escritorio/Drifters_position_october_2014.csv'
    
    df = pd.DataFrame({'Drifter name': file_names, 'deployment': initial_time, 'end time': end_time, 'start time': time_start, 'longitude': lon_start, 'latitude': lat_start})
    #df.to_csv(output_filename)
    
    lonlat = df[['longitude','latitude']]
    print('Day {0}'.format(dateref))
    #print(lonlat.to_string(index=False))
    dateref = dateref + datetime.timedelta(days=1)
    l2 = df[['start time','longitude','latitude']]
    print(l2.to_string(index=False))

